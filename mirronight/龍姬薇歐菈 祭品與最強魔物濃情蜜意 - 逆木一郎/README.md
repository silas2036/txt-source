# novel

- title: 竜姫のヴィオラ 生贄は最強の魔物と恋に落ちて
- title_zh: 龍姬薇歐菈 祭品與最強魔物濃情蜜意
- author: 逆木一郎
- illust:みやま零
- 翻譯: 梁恩嘉
- 錄入: 柯帝
- 圖源: 柯帝
- source: https://www.lightnovel.us/detail/1031869
- cover: https://i.imgur.com/Vb88tYR.jpg
- publisher: 尖端出版
- date: 2020-06-26T20:00:00+08:00
- status: 完結
- novel_status: 

## illusts

- みやま零

## publishers

- 美少女文庫
- 尖端出版

## series

- name: 竜姫のヴィオラ

## preface


```
　　「薇歐拉喜歡亞爾克唷，所以，來交配吧～」
　　獻給龍的祭品——騎士隨從兵亞爾克明明應該被吃掉才對，卻被蛻變為少女的龍姬薇歐拉跨坐在身上迎來初體驗。
　　每次都被天真無邪地硬上、纏綿熱吻、滿身大汗地高潮。
　　「肚子……滿滿的……總覺得好幸福。」
　　獻上第一次的龍之化身咧嘴傻笑，明明是最強的吃人魔物，她卻可愛地令人無法自拔！
　　愈是沉溺於快感就愈是眷戀，龍與人類索求著彼此的靈肉——


    作者簡介
    逆木一郎
    姓名：逆木一郎
    新銳作家。


    僅供個人學習交流使用，禁作商業用途
    下載後請在24小時內刪除，LK、TSDM、demonovel不負擔任何責任
    請尊重翻譯、掃圖、錄入、校對的辛勤勞動，轉載請保留資訊
```

## tags

- node-novel
- R18
- 美少女文庫
- 尖端出版
- 竜姫のヴィオラ
- 祭品

# contribute

- 柯帝

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1


## textlayout

- allow_lf2: false

# link
- [みやま零 - twitter](https://twitter.com/miyama0)
- [輕之國度 - 龍姬薇歐菈 祭品與最強魔物濃情蜜意](https://www.lightnovel.us/detail/1031869)
- [天使動漫](www.tsdm39.net)
- [美少女文庫 - 竜姫のヴィオラ 生贄は最強の魔物と恋に落ちて](https://www.bishojobunko.jp/c/item/82960021064750000000.html)
- [尖端集團 - 龍姬薇歐菈 祭品與最強魔物濃情蜜意](https://sppbuy.91app.com/SalePage/Index/6370212)