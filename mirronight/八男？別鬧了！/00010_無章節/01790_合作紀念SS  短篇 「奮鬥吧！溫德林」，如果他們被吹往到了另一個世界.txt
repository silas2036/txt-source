﻿合作紀念SS  短篇 「奮鬥吧！溫德林」，如果他們被吹往到了另一個世界

八男×銭同時発売記念コラボＳＳ「ヴェンデリン、奮闘す！」もし彼らが別の世界に飛ばされていたら。 八男×錢同時發售紀念SS 「奮闘吧！ 溫德林」如果他們被吹向到另一個世界

1.

「溫德林！你難道不拒絕我的命令嗎？」

「是的」

「坦率是件好事。我將加入鮑麥斯特騎士爵家諸侯軍，由我來指揮戰鬥！」

講述從已成死人的師傅那裡接受魔法特訓，正當我在想著今後還必須要努力進行魔法修行的時候，我……不，我們……被意想不到的不死者襲擊了。

鮑麥斯特騎士爵領位處在琳蓋亞大概的南端，聽說這裡除了鮑麥斯特騎士爵家的人以外就沒有其他人了，但是曾被哥哥說還有別的貴族領。

在毫無人煙的未開發之地，是不可能突然產生貴族領地的。

應該也不能是來自赫爾姆特王國的移民，大概是從南端海岸登陸後遷徙進來的吧。

自從上任布萊希萊德邊境伯侵略魔之森林以來，就不曾有誰去看過未開發之地的樣子了。

所以，才沒有注意到他們的登陸。

而且，我們鮑麥斯特騎士爵家，打算和謎之勢力做第一類接觸。

但是，庫爾特他並沒有考慮要和對方進行友好性質的協商。

對他來說，北上而來的那些人只是侵略者。

「庫爾特哥哥，你知道移居到未開發之地的那些人的底細嗎？」

「知道！ 但是，未開發之地是我們鮑麥斯特騎士爵家的！

應該要向侵略者降下制裁的鐵鎚！」

「那麼他們的戰力呢？

只靠我們能贏嗎？」

和侵略者一戰這件事，庫爾特奇妙地顯得幹勁十足。

作為貴族我可以斷言我們家肯定是在最基層的位置，目前好像是第一次使用貴族的權限。

在那興高采烈的樣子下，庫爾特把四周的情況都無視了。

還有，離預定時間不到一個月就要前往王都的埃裡希哥哥他們都急忙來參加諸侯軍，頭腦聰明的他因為是庫爾特的參謀所以顯得很冷靜。

雖然還分派了裡正的克勞斯來做相同的工作，但是因為庫爾特很討厭這二個人，所以將他們的忠告全都否決了。

即使告訴他要為了試探對方的戰力而進行偵查，卻因為會將不多的戰力分散掉只能放棄了。

反正只要單純地進行防衛就行了，所以不需要去白費力氣。

「你們！ 我們是不可能投降的！

一旦敵人攻撃這裡就以防守來抵禦，視情況進行反擊。若是能搶到那些傢伙們的移居地，可是能與我們爆麥斯特騎士爵家的躍進相連的！」

不，要是能這樣是就好了。

總之，北上而來直逼到廣闊的未開發之地的北端鮑麥斯特騎士爵領附近的敵人，兵力上是不可能在我們之下的。

那件事，連小孩子都懂。

「多少會有兵力上的差距，但是我們還有溫德林的魔法！

父親也前去向布萊希萊德邊境伯討救兵了！ 我們一定會贏的！」

呿，不知為何庫爾特要用很了不起的態度說話，而父親正去向布萊希萊德邊境伯討救兵當中。

雖然有想過『為什麼原本該站在第一線的不是父親？』，但這卻是鮑麥斯特騎士爵家悲哀的現實。

布萊希萊德邊境伯家的人認識的爆麥斯特騎士爵家的人，只有父親和克勞斯而已。

即使庫爾特去討救兵，最壞的情況有可能誰都不認識他。

搞不好，也許還會被對方當成冒牌貨來看待。

因為這樣的理由才會由庫爾特來指揮軍隊，不過，我認為父親的選擇是錯的。

大概，除了庫爾特以外所有人都是這麼認為的吧。

「那個……這種情況下，由溫德林大人前去討救兵會不會比較快？」

特別是克勞斯，絕對是在心裡面將庫爾特當成笨蛋來看了。

由我使用『飛翔』魔法去叫來援軍確實會比較快。

即使小孩子都知道，但不論是父親或庫爾特都不打算這麼做。

算了，父親的考量是身為這塊領地最強戰力的我若是不在的話，可以預料到會引發領民們的恐慌，或許也有這樣的理由。

話雖如此，還只有六歲的我是最強的戰力……。

「讓溫德林去找來援軍？

哼！如果膽小到逃走的話該怎麼辦？」

很可惜的是，庫爾特在懷疑我。

到目前為止幾乎沒有與他接觸過，但是在非常時期最緊要關頭的時刻他會是個不適合做家主的男人。

不管在那個世界，最高的負責人是不該在眾人面前公開說出不信任身為自己部下的人。

在戰爭開始後，可是會讓鮑麥斯特騎士爵家諸侯軍士氣大跌的。

「作為現實的問題，在領主大人到達布萊希萊德邊境伯領會花上一段時間的。那麼，這段期間我們抵擋得住敵人的進攻嗎？」

「假如戰況朝向不利發展的話，逃進森林進行抵抗就行了。聽好了，我們赫爾姆特王國的領地遭受到敵國的侵略。王國肯定會派遣援軍過來的吧」

面對埃利希哥哥的發問，庫爾特輕佻地回答了。

雖然很花時間，不過，王國會派遣援軍而來的可能性很高。

在那之前，不保證鮑麥斯特騎士爵領不會被完全佔領。

話又說回來，庫爾特真個在關鍵時刻沒什麼用處的男人。

你啊會是個光說不練的參謀嗎？

不，他們幾乎沒有學習過吧……。

逃進森林哩，即使在我們家所佔有的森林中熊和野豬對領民們來說可是威脅喔。

首先，非常時期該有的準備什麼時候有在森林裡準備了呢？

要讓女人、小孩、老人家在森林中露宿。

這麼做，在王國軍到來為止得過上好一段時間。

肯定會有人因此而生病甚至出現死者吧。

而且，還要給予八百位領民們飲食，並不存在能對敵軍的搜索產生隱蔽的森林。 (註:原文是誤魔化せる。欺騙的意思)

「要是變成這樣，姑且讓領民們投降會不會比較好？

只靠人數少的諸侯軍在森林裡潛伏，在和從王國而來的援軍一起進攻奪回鮑麥斯特騎士爵領的話……」

「你覺得能用這種膽小鬼的戰術嗎！

不要用你那張孩子般的臉提出這種無用的策略啊！」

突然間，我被庫爾特打倒在地了。

因為我的身體還只是個孩子，是抵擋不了他那一瞬間的暴力的。

嘴巴裡好像因為有傷口，出現了血的味道。

就算我這麼說，也不該突然間揍過來吧。

我和你，不管到哪就是沒有緣。

「威爾！ 庫爾特哥哥！

即便威爾還小，還是為了領民們的安全才提出這個方案的喔！」

「庫爾特大人，如果用溫德林大人的計策的話，很有可能能對敵軍的補給帶來負擔。他們，也有會害怕我們的反抗而不去進行剝削的可能性」

倒在地上的我，馬上就被埃裡希哥哥浮起來了。

話說回來，如果發生這種狀況我覺得投降會比較好。

假如能在對方沒有出現犧牲就投降的話，說不定也能阻止調敵人的掠奪。

在這種中世紀歐洲風的世界是不能保證有用的，但如果抵抗的擦槍走火而出現犧牲，最壞的情況下會全死的。

其實我也可以自己一個人逃走，但我想我會對被留下來的領民們和埃裡希哥哥他們產生罪惡感的。

「我們鮑麥斯特騎士爵家，要作為現今赫爾姆特王國的先驅，對抗這些神秘的敵人！

就算會出現很多的犧牲，還是要抵抗到最後！」

唉，你這傢伙是不會進到這些犧牲裡面的吧。

「那麼。首先就是要掌握敵人的戰力。偵察是有必要的吧」

「赫爾曼！ 你對我有意見嗎？」

「意見是在那種問題之前，這可是軍事常識啊」

「無奈！ 溫德林，你去偵查！」

人類，只有在走投無路的時候才會露出本性。

平時會好好地聽從父親所言的庫爾特，是個會逞口舌之勇而什麼事都做不到的人。

只會搬出父親的名字來狐假虎威而已。

其實是為了隱藏內心的害怕，所以才會顯現出很強烈的怒吼出來。

唉，實際與他接觸過後旁人都會認為他只是個麻煩而已。

倉促之下所編成的諸侯軍也是由在所有能夠指揮的兄弟之中體型最壯碩也很有威嚴，平時即使人數不多還是會進行警備和驅除害獸的赫爾曼哥哥來指揮，而底下的保羅哥哥和赫爾姆特哥哥則進行輔佐，埃利希哥哥則是擔任參謀兼軍政官的作用。

而人在這裡的裡正克勞斯，和我們同父異母的兄弟也來參加諸侯軍併進行輔佐。

庫爾特因為代表父親所以是這裡最大的，不過，在揍我的時候只是好看用的人罷了。

我認為不論是誰，都很討厭為了他而死。

對於庫爾特的勇敢抵抗宣言，他作為軍人是不可能替他所具有的鮑麥斯特騎士爵家的繼承人帶來實績的，因此才會很少有人願意服從他。

總覺得，這傢伙會犧牲他人來讓自己活著，之後還會出現向前來的王國援軍誇耀自己的功績的情節。

假如他是個擁有內心的真心話不會被看穿的器量之人就好了，但是最糟糕的情況是父親算錯了人，應該除了庫爾特以外的眾人都是這麼覺得的。

「在幹嘛？ 快去啊！」

雖說我還在學習魔法當中，不過，我是鮑麥斯特騎士爵家最強的戰力。

如果使用『飛翔』，會被認為要對敵軍進行偵查是很容易的事吧。

「我走了」

這下子就不會因為看見這傢伙的臉而感到不爽了，我用學來的『飛翔』飛向南方了。

來到離鮑麥斯特騎士爵領以年約二十公里的平原，神秘的敵軍部隊在此地建起陣地來了。

「敵軍的數量……雖然很明白，但也該好好地接受教育吧……」

以不被敵人發現的方式在計算著軍隊的人數，很不湊巧的是我沒有作為軍人的經驗。

數千人……還是已經超過一萬人了呢？

不管是哪個，只要鮑麥斯特騎士爵家諸侯軍一遭遇上的話是會被一擊粉碎的吧。

這下子，連投降幾乎也很渺茫了……當我在思考的時候，看來是被敵方給發現了。

雖然我將魔力隱蔽起來了，但是還不是很成熟。

從敵方陣地中有一位穿著長袍之姿中年男性，用『飛翔』飛來這邊了。

那支壯盛的大軍。

還是有很寶貴的魔法使。

「小孩子？」

確認過我的身姿的魔法使，因為我是小孩子而解除了警戒。

即使如此這位魔法使，雖然體型很壯碩但很明顯得是一位日本人吧？

慢著，現在還不是時候。

逃的了嗎雖然覺得很危險，不過，對方因為我是小孩子而大意起來可說是萬幸。

即使看起來是老手，如果因此而疏忽大意就危險了。

『威爾，大意時不論是多強的人都會在一瞬間變成弱者的』

想起師父的話，我讓拳頭集中著『魔法障壁』然後衝進敵方魔法師的懷裡了。

雖然他也展開了『魔法障壁』，但是比魔量是我有勝算。

由於蓄滿在拳頭的一擊，把敵方魔法使的『魔法障壁』打碎了。

「怎麼可能！ 明明年紀比我還年輕！」

「有空隙！」

繼續撲向『魔法障壁』損壞掉的魔法師的懷裡，然後在他胸口的部位展開了風魔法。

說到底我是原平成年代的日本人對於殺人是很猶豫的，只是吹飛他並留下很輕的傷害而已。

因為施加的一擊打在胸口上，被吹飛出去的魔法使當場雙膝下跪動彈不得了。

「趁現在！」

趁敵方魔法使無法動彈之際，我趕忙用『飛翔』逃回鮑麥斯特騎士爵領了。

2.

「陛下、夫人、宰相大人，因為意想不到的狀況屬下失策了。真是對不起」

「唉，人沒事就好了」

「居然，被那樣的小孩子單方面擺佈了……取回這個面子前，在下松永久秀，打算歸還足利王國首席魔法使的地位……」

「不行。沒有其他適合的人」

朝琳蓋亞大陸進攻和遷入雖然很順利，不過，卻遇上了意外的阻礙。

打算佔領擁有這唯一擁有未開發之地的貴族領而讓軍隊北上的時候，當時發現到有一名魔法使在尋找我方的軍隊，因此為了抓住他而派出了首席魔法使松永久秀，但是卻因被反擊而讓對方逃走了。

而且，根據久秀報告，那名魔法使是一位只有五、六歲左右的孩子。

「出現天才兒童了呢。哥哥。久秀，你認為那孩子的實力有多高呢？」

我那做為宰相的弟弟清輝，對那位魔法史的小孩頗具興趣而問起久秀了。

「以他的年齡，就已經超越我的魔量非常多了吧。掉以輕心的我說什麼都沒用，但是魔力的使用和戰鬥方法卻還不成熟。他的魔力今後還會處在成長期。肯定會成為一位難以想像的魔法使」 (註:魔力量は餘裕で超えて，怕大家忘記這部作品的設定是MP=攻擊力。同樣是"美拉"MP越高攻擊力越強)

「嘿耶，那還真是厲害呢。沒辦法拉攏過來嗎？」

「連我都想他當我的女婿了」

對秋津洲王國的統一做出極大的貢獻，這是從首席魔髮師的永秀那裡所得到的評價。

這樣，說不定是真的(天才)。

「問題是，赫爾姆特王國這邊還有像他那麼厲害的魔法使嗎？」

作為我的正妻，王國軍最高司令官的今日子，對赫爾姆特王國魔法使的人才濟濟開始警戒起來了。

「大嫂，不用太擔心喔」

「是嗎？ 小清」

「他是在做事前的偵查吧。琳蓋亞大陸也一樣。魔法使可是很寶貴的。只不過，對他所擁有的魔量可能會對我方很不利。在琳蓋亞大陸，連目前都還能定期發現具有上級水準的魔法使」

「上級嗎……。在我們秋津洲島上，可以說是傳說了吧。因為持有中級程度的我就已被當成天才來看待了」

距離琳蓋亞大陸最南端往南約三百五十公里的海上，這裡有座名為秋津州的島嶼。

人口大約三十萬人，在我們到來之前是不存在統一的國家，而是將土地分散交由數十名貴族來統治。

雖然還不到以血洗血的戰亂之世，不過，領主之間時常會發生小摩擦，在分散統治的影響下也讓有效率的發展受到了阻礙。

據說他們，在遠古時代的琳蓋亞大陸的北方有構築過一個名為秋津州共和國的保護國國民的後代子孫。(註:保護國，保護國是非獨立國的一種，也是殖民統治的一種特殊形式。帝國主義國家為了掠奪原材料產地和國際市場，用強力的手段迫使弱小國家，落後國家同他簽不平等條約，以「保護」為名，控制和吞併弱小國家。這些弱小國家就稱為保護國。宗主國與保護國之關係為國家之間的不平等關係之一，弱勢國家因簽訂不平等條約而接受強勢國家的軍事保護，以致無法自理內政、外交事務，甚至喪失主權。)

一萬多年以前，以繁榮自豪的古代魔法文明時代一夜之間滅亡了。

暫且，琳蓋亞大陸成了無法住人的土地，他們靠著船逃到了名為秋津州的島上後就定居下來了。

其實，預定是想在琳蓋亞大陸穩定下來以後馬上回去的。

因為還有同胞往北方而逃了，也很在意他們過得如何。(註:這裡是解釋魔族之國在後來分成瑞穗伯國和秋津州共和國的由來)

然而，由於秋津州島的政治產生混亂統治工作回到由王族和貴族來統治，因此這種統治方式也持續在崩壞並處在割據的狀態。

最後，這裡被我們統一了。

運用了巨大的宇宙船神奈川，以及科學的力量。

我足利光輝和妻子今日子、弟弟清輝三人是從事往來宇宙間的運輸業。

然而在工作中被捲入異次元漩渦，注意到的時候已經來到一個有著魔法的西洋幻想風的世界。

但是，不知為何最初接觸到的人卻是個日本人。

他們，似乎也因為目擊到從宇宙船走下來的我們和自己長得很相似而受到衝擊了。

雖然之後還發生過各種各樣的波折，但卻也因我們是從神船上走下來的而被當成神之使徒來看待了，然而靠著驅使神奈川的科學利和生產力對秋津州島進行統治，時至今日可以說我成為這個國家的國王了。

雖然也有極少數的反抗者，不過，他們都已經不在這個世上了。

大部分的住民們，因為生活變得富足而對我們很支持。

但是，統一之後成立王國的關係使得統治和開發變得很有效率，還因糧食的情況和醫療技術進步的結果，使得人口呈現出爆發性的成長了。

再繼續這樣下去，秋津州島的人口會出現過於密集的狀態。

於是，才會打算移居到琳蓋亞大陸的南端以及周邊的諸島……他們還說過計劃要返回故里。

其實到達琳蓋亞大陸南端為止的海域都有著大量的海竜巢穴，所以那也是他們無法返回大陸的原因。

由於遷移到島上居住而使得技術和魔法衰退下來的他們，是沒有擁有能夠突破海竜巢穴的力量的。

海竜隨著我們的高效率而被消滅，才打開了通往琳蓋亞大陸的道路。

整頓好並登陸上舊秋津州共和國領時，發現這裡已經被濃密的叢林所包圍，同時還畫成了魔物的樂園。

『大哥，這裡是幻想世界耶。有怪物和竜喔』

很可惜，靠我們現在的力量是不可能完全將牠們驅逐光的。

魔物不會比海竜來的弱。

我們好不容易才讓士兵們裝備了鐵砲和大砲，同時間能與竜對抗的魔法使卻是處於人手不足的狀態。

由於長年窩在秋津州島的關係，我們足利王國的國民中魔法使的人口極端的少。

那不足的部分，是靠神奈川和科學之力來彌補的。

不能太勉強自己。

話雖如此，今後是做不到和那位擁有可怕魔量的天才魔法少年一戰的。

「那位魔法使，沒頭沒腦地將大本營當成目標就危險了」

「是啊……還會有其他魔法使嗎？」

「應該沒有了吧。因為即使在琳蓋亞大陸，能夠好好地使出攻擊魔法的魔法使數千人中也只會出現一個」

事前有透過偵察衛星和無人機進行情報收集的清輝，自信滿滿地回答了。

掌握著情報。

沒有比這件事更有利的了。

久秀以下追隨我們的秋津州島的住民們，都擁有能夠驅使科學的壓倒性力量。

雖然有魔法使會很方便，但是出現率是個問題。

擁有科學之力的我被捧為國王也好，妻子今日子成為王妃也好，弟弟清輝成為宰相也好，儘是托科學的福。

「不過，太可惜了」

「可惜什麼？ 清輝」

「那名魔法使是男的。在秋津州島上也是魔法使幾乎都是大叔或老爺子，現在我比較想看到美少女魔法使啦」

加上，這幾年秋津州島幾乎沒有出現過女性的魔法使了。

也有這樣的原因，總覺得島上的居民男尊女卑的傾向很強。

「「就不會想點別的啊」」

「真是一對無趣的夫妻啊」

清輝應該也不會特別喜歡魔法少女的動畫吧……。

而且假如敵人那邊有魔法使的美少女，還是會來殺我們的喔。

「到時候，一邊處在敵對的同時一邊慢慢地讓對方陷入戀愛之中的是敵人的魔法使與宰相的青年」

「「你是不會有戀愛的對象喔！」」

「你們夫妻還真是同聲一氣啊！」

我和今日子，同時對清輝吐槽了。

「小清，看清楚現實吧……」

面對清輝的妄想，今日子吐槽了。

因為是平時見慣的景象，久秀他們已經掌握住忽略的能力了。

「久秀，如果靠所有魔法使來抵擋的話應該沒問題吧？」

「是得。他的防守會變得很薄弱，不過……」

「那就靠鐵砲隊和砲兵部隊來抵擋吧。據說在大聯合山脈南部持有領地的貴族只有鮑麥斯特騎士爵家。人口大約八百人。魔法使，只有那名孩子。如果能準確應付的話，剛開始是不會輸的。之後，就看久秀他們如何壓制那名孩子了」

事前，清輝就掌握了敵人的情報。

因為，直到剛才偽裝成鳥的無人偵察機都還在飛著。

「小輝，好想要那名孩子呢」

「沒有犧牲要抓住他是很困難的吧……」

「或許會意外地不困難」

「為什麼？ 今日子」

雖然久秀統領著足利王國的魔法使，卻輸給對方了……。

我，認為要生擒是很困難的。

「那名孩子，雖然有做為魔法使的才能，不過，很明顯地經驗不足喔。而且，並沒有殺了久秀」

「對。當我自己的『魔法障壁』被打破，被衝進懷裡之時，我已經過好死的覺悟了。但是……」

「中途給予不痛不癢的傷害就跑了。我想那名孩子是來偵察的。以帶回情報為最優先，但是卻陷入了不給久秀最後一擊麻煩狀況了。不過，沒發生這樣的情況是，那名孩子在戰爭中既沒有對人戰鬥的經驗也沒有殺過人。嘛，畢竟他只是個孩子。和我們家的愛和太郎年紀差不多吧？」

「倒不如說，還更年輕」

那名魔法使的少年大概約五∼六歳，我們家的長女愛是八歳所以多二歲比較年長。

「因為具有魔法使的才能才從軍的吧。好可憐喔。久秀和魔法使部隊就來當那孩子的對手。好好地抓起來吧」

「就照大嫂的意思做吧。其他的鮑麥斯特騎士爵家諸侯軍，請儘量不要讓他們付出犧牲讓他們投降吧」

只有他們被迫居住在這種偏僻的地方，肯定對赫爾姆特王國的忠誠度是零吧。

如果能加入進來的話就好了。

「大哥，如果把未開發之地全境都控制住的話，就能讓神奈川移動到這裡來囉。把新的王都建設在這裡比較好」

「因為秋津州島很窄啊……沒有開發的餘地還真是麻煩」

雖說還不至於到達會出現危機感的狀況，但是那是時間早晚的問題。

必須要將這片未開發之地，變成足利王國的根據地才行。

「我等民族之所以能回故里。這也是多虧神之船和足利王國的福」

「足利王國萬歲！」

「「「「「萬歳！」」」」」

這些人，因為神奈川突然之間出現在島的附近，而真的將從那裡出現的我們當成神明的使者了啊……。

只不過，偶爾會因忠誠心太強而冷場情況出現。

「只有那名孩子要好好地抓住，剩下就是進行展現我們的力量讓對方投降的方針」

好不容易來到這裡，儘量不要和魔物及野生動物以外展開戰鬥併進軍。

我不喜歡戰爭，希望能在沒有犧牲的強況下平定未開發之地。

3.

「庫爾特哥哥，打算進行抗戰是不可能的喔」

「是啊。敵人可能有數千甚至超過一萬人的可能。士兵的質量也是，我們這邊大多是農民和獵人喔。絕對贏不了的」

「只能祈禱對方會有理性的人在。不去和他們談一談嗎？」

「不行的話，可以逃到布萊希萊德邊境伯領吧？

不，讓女人和小孩翻越大聯合山脈會很辛苦」

「庫爾特大人，根據溫德林大人的報告，敵人可能擁有許多魔法使。針對這點，我們這邊只有溫德林大人一個人。因此，是無法作為王牌的」

甩開敵方魔法使逃回鮑麥斯特騎士爵領的我，儘可能報告出我所知道的情報。

聽完那份報告的埃裡希哥哥、赫爾曼哥哥、保羅哥哥、赫爾姆特哥哥，以及克勞斯等諸侯軍幹部的全員都陳述反對進行抗戰。

不管怎麼打都看不見能贏的希望，所以(抗戰下去)也只是讓我們出現許多平白的犧牲而已。

雖然這是連小孩子都懂得理由，不過，卻有唯一一個人主張要進行抗戰。

那就是作為父親代理的庫爾特。

「身為貴族，不可能一戰皆無就投降或逃走的！

你們全都得到了膽小鬼的病嗎？」

與其說是膽怯，還不如說是立場不同吧。

庫爾特身為繼承人，更得堅持身為貴族的立場。

但是，眼下有著戰力上的差距。

如果是父親的話他是會投降的吧。

其實讓年輕的庫爾特去當請求援軍的使者會比較好，不過，布萊希萊德邊境伯家有勉強記得住父親長相的人在，但應該沒有人認識庫爾特。

因為有被當成冒牌貨的可能性，所以才會由父親來當使者。

然而，從當前庫爾特的言行來看人選相當失敗。

「以這樣無謀的抗戰是會讓領民出現損害的。讓領民投降，只有我們潛伏起來等待援軍的做法會比較明智一點吧」

克勞斯的獻策，我覺得還蠻務實的。

假如以幾個人，逃進和大聯合山脈接壤的森林裡的話，應該能爭取到幾個月的時間。

「你覺得我會認同那種逃避的策略嗎！

確實領民們作為士兵的經驗很少！ 但是，這也是為了守護故鄉！一定能發揮出超越實力的力量吧！」

人，有時候如果被逼急的話，聽說會特意往陷阱裡去。

現在的庫爾特就是處在那種狀態下。

因為他被父親任命為代理人而產生了責任感，因此他才會非常固執。

如果在父親回來為止都沒有進行抗戰的話，認為之後會被廢嫡吧。

不管怎麼看，對下面的人來說都很麻煩。

「大哥，有幹勁是好事，但不是那種層次的問題喔。戰鬥是靠數量，我們的人數可以說是很致命的少。這時候姑且投降比較好，就只靠我們潛伏起來吧」

「赫爾曼！ 你這傢伙啊ーーー！要是那麼做的話，之後可是會追究我的責任你是打算自己成為繼承人吧！」

「為什麼會變成這樣啊？」

果然，這樣子什麼事都做不了了。

在庫爾特作為代理家主的時候，他說什麼就是不肯投降。

「因為敵人的人數多所以營不了？

愚蠢！ 我可是有必勝之策啊！ 喂！ 溫德林！」 

被點名的時間點上，我就已經明白必勝之策的內容了。

沒錯，我是個非常倒楣的人。

「讓溫德林去討伐敵人的總大將就行了！

那樣一來，剩下來的人就只是一群烏合之眾而已！ 剩下來的人不管是討伐或是讓對方投降，都會成為我鮑麥斯特騎士爵家躍進的起始！」

「那簡直在說夢話……」

面對那支大軍，我不認為梟首作戰會成功……。

「溫德林！ 去吧！」

在這個時間點上我一個人逃走就行了，但是在內心憨厚的原日本人的這個時間點上對於無法割捨感到很困擾。

而且，不論是什麼情況，對領民和家族見死不救而逃走的貴族之子會過著平凡的生活怎麼看都很奇怪。

即使成為冒險者，被上面的人注意到的話，說不定會因被週遭的人說壞話而感到心裡不舒服。

「（無奈……才在想好不容易才會使用魔法，活不滿一年就要死了……但是……」

對要這樣就去死感到很生氣。

雖然不習慣殺人，不過，我感覺只有一個人我無法原諒。

這傢伙為什麼不去死一死啊。

雖然我不能直接出手，但是我覺得他的下場會很悽慘。 (註:必ず慘めな思いをさせてやる。隱含的意思是指要自己沒死將來有可能自己會動手)

「我明白了。那，我就去殺敵人的總大將吧」

「呵呵，那麼做才是魔法使。成功的話，就讓你來當鮑麥斯特家首席私人僱傭的魔法使吧」

那種地位我覺得就算死我也不要，此刻還是老實地低下頭讓庫爾特疏忽大意就好。

4.

「威爾，對不起……」

那麼，雖然我必須要深入敵陣，不過，因為還是小孩子身體很小，所以穿不了師父留下來的長袍。

如果能穿上去的話防禦力就大大不同了，但是……。

連修改的時間都沒有，只能直接去了吧。

雖然有鎧甲，但是應該不可能會有小孩子能穿的。

因為那種東西，只有王家或大貴族家才會有。

當我在進行有限的準備之時，看見埃裡希哥哥的身影，然後他突然向我道歉了。

「這不是埃裡斯哥哥的錯喔」

「不，畢竟逼迫還是小孩子的威爾去那麼危險的地方。就算做出反對，但因為改變不了什麼所以我也是同罪」

對帥哥的埃裡希哥哥意想不到發言……。

如果我是女的肯定會喜歡上你的。

「放心吧。我不覺得自己會死。還有，我有一件不講理的事情。希望埃裡希哥哥成全」

「把庫爾特排除掉對吧」

一瞬間，埃裡希哥哥的臉上露出殺氣了。

對庫爾特直呼其名，表示很厭惡他吧。

「他，會為了自己而打算殺了別人。根本沒有當家主的器量」

「是啊。情況變得有點麻煩。會儘可能嚐試把他抓起來，不行的話就由我來殺」

接在埃裡希哥哥後面，赫爾曼哥哥也露面了。

「那件事情你是做不到的，不過，讓溫德林來當家主會更好你就好好去做這件事吧」

「赫爾曼哥哥，不行啊。你是不可能殺得掉庫爾特的」

然而保羅哥哥更接在後面出現了，只是表情不太好看。

「不行啊……」

「那個妄想，為了鼓舞士氣是能以這樣的名義向領民們說的。不過，一部分的領民會被慾望所誘使，而將庫爾特給圍起來保護著的」

雖然這麼說很失禮，不過，還是會有笨蛋會被庫爾特的煽動所釣上。

在這樣閉鎖的領地內，也是會有像這種不懂事故的笨蛋在的。

如果能冷靜地計算一下或許會發現自己選錯了。

「靠溫德林的魔法總會有辦法的。再說，或許溫德林的魔法很厲害，但是對方也有許多魔法使在對吧」

「知道那件事的大多數人，是不會隨庫爾特的煽動起舞的」

「如果是少數人，最壞的情況就把他們全砍了！

比起就這麼抗戰下去犧牲還會來的更少。話說回來，赫爾姆特呢？」

「拜託他去監視庫爾特和他的跟班了。真是的，明明他會在庫爾特的結婚典禮完以後前往王都……」

「保羅哥哥，我也是喔」

保羅哥哥、赫爾姆特哥哥、埃裡希哥哥預定都在庫爾特的結婚典禮結束後前往王都。

然而在那之前就被捲入戰爭之中，只能說很不幸。

「預定作為從士長留下來的我，只能對你們說我的悲哀。不過，還有投降到對方那�去當官的選項」

「赫爾曼哥哥，去當敵國的貴族如何？」 (註:それは王國貴族としてどうなんだ。這句直翻保證大家看不懂，因為這邊的王國指的是"足利王國"，更不要說這部分的對話BUG滿滿，對主角一行人來說當下都還不曉得對方是君王制、帝王制、還是諸侯分治)

保羅哥哥，對說出想要去敵人那邊當官的赫爾曼哥哥提出忠告了。

「保羅哥哥好厲害啊。充滿著作為王國貴族的氣概。換做我就沒辦法了」

「我也我自己的生存之道，作為王國貴族的自覺很薄弱喔。預定到王都再培養」

赫爾曼哥哥和埃裡希哥哥一樣，連我也是。

「我，就傾注全力來爭取時間吧」

因為我不想死，所以只能儘量爭取時間，希望在這段期間內將庫爾特排除掉並投降。

以後的事只能以後再打算了。

就算假使鮑麥斯特騎士爵家滅亡了，我一個人能活著就好。

「我走了」

「對不起啊，威爾」

「我一定會砍了庫爾特。儘量早點結束」

「克勞斯也是站在我們這邊。庫爾特已經完了」

我在哥哥們的送行之下開始往神秘的敵軍那裡進攻了。

5.

「去吧！」

在庫爾特的命令下我前去討伐敵方的總大將了。

雖然認為就算把對方幹掉也解決不了一切，但無法預料到家主被討伐後繼承人和家臣們未來會不會報復呢？

還有，雖然得到了消除魔力可以從極近的距離進行奇襲的策略。 (註:やはり，在這裡是作為雖然...所以的用法。這二段會這麼翻只能問作者幹嘛這麼寫)

但是，我還不是很熟練。

還不清楚對方的魔法使水準到哪，不過，要是來十位中級程度的老手就有辦法輕易將我制服住吧。

「這樣的話！」

我衝到敵軍面前，在那裡堂堂正正地自報姓名了。

「我正是溫德林・馮・培諾・鮑麥斯特！ 侵略者們啊！ 別再往前進了！」

果然，敵人的數量有數千到一萬人。

原本未開發之地就毫無人煙，所以他們都只是在與魔物或野生動物戰鬥而已。

總之，他們幾乎沒有受到損害。

實際能打仗的戰力最大極限約二百人左右的鮑麥斯特家是不可能打贏的。

加上，未開發之地的南方並沒有人居住。

因為並沒有能夠下決定的依據，所以我認為這不僅是我還是所有赫爾姆特王國的人們的疏忽大意。

父親雖然去向布萊希萊德邊境伯領請求援軍了，但是會確實地將援軍帶來嗎？

要是王國決定要將未開發之地不列入領土範圍而放棄掉的話，那一切都完了。

不清楚敵人都是怎樣的人，總之為了留下談判的餘地，只能爭取時間讓其他的哥哥們能將極力主張抗戰到底的庫爾特給排除掉。

「小子！ 你出現了啊！」

看樣子，在自報姓名結束前似乎阻止掉了向驟雨一樣箭矢發來的結局。

剛才，為了閃躲有看見我的魔法打中了一位中年魔法使了。

果然，他就是這支大軍中最厲害的魔法使。

雖然我感覺到他有著老手般的身手，不過，魔力卻比我還少。

以師父當基準他應該有中級的水準。

由於器合所以我有跟師父同等的魔力，從那之後開始我都魔力每天都有稍微在成長著。

如果不多想全力一戰我是可能打贏的，不過，這支大軍的魔法使應該不只他一個人。

遺憾的是我探測但有幾十股魔法使所發出的魔力，他們陸續在中年魔法使的後面聚集起來了。

「（這樣下去是不行的……只能儘量爭取時間）對付一個孩子就要出動幾十個人啊。這支大軍的總帥，是一位相當膽小的人吧！」

「你說什麼！」

「明明只是個孩子！」

幸運的是，在我的挑釁下好幾名魔法使顯著很激動。

如果能就這麼依序決依勝負的話就能爭取到時間了。

但是，就算一切順利也不保證我能活著。

被吹飛到這個世界已經過了好幾個月，明明好不容易才學會魔法說不定我將要死在這裡……。

這是內心是平成日本人的我所在思考的事。

如果能對領民們見死不救而逃走的話，或許會有快樂的人生在等著我。

好想要顆鐵一樣的心就算做了這樣的事也不會感受到罪惡感。

「小子，那樣的挑釁是行不通的。你在爭取時間嗎？

算了。如果你不在的話，留下來的領民們都會老實地投降吧」

「然後，你們打算進行掠奪、殺戮和強姦嗎？

好野蠻啊！」

「小子，看來你雖然還是個孩子卻知道一些困難的詞句啊，但你現在可是在侮辱我們足利王國喔！」

「贏的話，政府軍之後多少會進行粉飾的吧」

「哎呀，忘了自我介紹。我的名字是足利王國的首席魔法使松永久秀」

「誒？」

「怎麼？ 小子。難道你知道我的名字？

我們，明明才頭一次和赫爾姆特王國的人接觸」

我也不是那麼嫻熟歷史，不過，在小孩子的時候卻玩過某款戰國系列的模擬遊戲。(註:這裡是指信長之野望這個遊戲)

就算有才能也會馬上背叛，想起來松永久秀這個名字的評價非常不好。

最後好像是和茶壺一起被炸死的吧？

當然他只是同名同姓，不過，好想在做什麼警戒的樣子。

感覺會做出甚麼卑鄙的手段，而我只能做好警覺了。

然後，那個懷疑成了事實。

突然間，『碰―ーー』的醫道輕輕的爆炸聲正面朝『魔法障壁』侵入過來了。

「嘖！ 子彈啊！」

意外地，自報是足利王國的大軍居然有配備鐵砲。

以防萬一幸好有預先準備好厚實的『魔法障壁』了。

與此同時，理解到我被逼入相當不利的局面了。

『魔法障壁』雖然對子彈也有效，不過，或許會經常被狙擊所以不得不總是展開強力的障壁來抵禦。

就算我的魔力再多，但對手的人數很多。

我的魔法不用多久就會被耗光了吧。

「這樣的話！」

為了防止敵軍的狙擊，我闖入待在松永久秀後方的魔法使集團了。

如果能形成混戰的話，就能防止狙擊了。

「什麼！ 好快！」

「太慢了！」

即使身體還是孩子，要是機動力能在魔法之上就好了。

首先為了減少對方的人數，我讓『爆碎』魔法在初級魔法使們所聚集起來的地方爆炸開來了。

這是用爆炸來吹襲地面，不僅是暴風連土石都能作為碎片砸向敵人的魔法。

以為我中心半徑二米的地面被爆炸所吹襲了。

「王八蛋！ 是誰狙擊的！」

不愧是首席魔法使。

連其他我認為是中級水平的魔法使們都冷靜地展開了『魔法障壁』，不過，大約十名初級魔法使則都被爆風所吹飛，還挨了許多土石的碎片全都不能戰鬥了。。

雖然沒死，他們暫時應該沒辦法參與戰鬥了。

「（先把人數減少！）」

一開始，就將不是很強的初級魔法使當成目標。

迅速移動到他們的懷裡，然後把雙手放在對方的腹部將魔法放出。

這麼座雖然不會死，不過，因為受到超過外表所能看見的傷害，他們都當場無法動彈了。

其他，還用雙手做出『龍捲』將人往高空吹飛上去，大約過了三十分鐘已經有超過二十名以上的魔法使無法戰鬥了。

很可惜，沒有殺掉是因為我沒有殺人的經驗所以讓我很猶豫，假如埃裡希哥哥他們投降了，他們是有可能因為我殺了魔法使的緣故而被面臨被報復的危險。

「小子，你真敢做啊。果然你魔量深具壓倒性。如果給你十年，我們肯定會全滅」

「……」

「而且，就算我們都全滅了足利王國還是很強」

因為有裝備鐵砲吧。

說不定他們在琳蓋亞大陸上是最精銳的。

既然有鐵砲也許也會有大砲，在繼續這麼打下去情況會越來越不利吧。

即使如此，直到鮑麥斯特騎士家諸侯軍發起下個行動之前，我只能儘量爭取時間。

「確實我們的魔力很少。但是，還有這種戰法！

上囉！」

「「「「「噢喔！」」」」」

十名左右的中級魔法使把我圍起來，不斷地射出『火球』、『冰彈』、『土礫』、『風刃』。

一個接一個雖然威力很輕，但是數量卻很多。

再加上他們的合作很好，才沒有發生同室操戈的情況。

由於全都打中了『魔法障壁』，使得我的魔力被慢慢地奪走了。

我，除了一直維持『魔法障壁』外就什麼事都做不成了。

果然我還不成熟，才會被松永久秀瞄準那道間隙。

「嘖！」

「好驚人的魔力啊。真有雅興」

「是啊，這樣的年紀就有這種實力了」

「不能掉以輕心，把他的魔力給耗光」

即使從魔法袋內取出魔晶石，在採取行動的瞬間卻因『魔法障壁』消失的關係什麼事都做不了了。

連我自己都確認到魔力正慢慢地在耗盡當中。

然後，在戰鬥開始後過了一個小時。

魔力已經耗盡了，意識跟著朦朧了起來。

「真有耐性啊。小子。值得誇獎」

「謝謝……」

不行了。

已經沒有力氣連意識也要失去了。

想辦法不暈過去，我集中起精神。

「為什麼還要抵抗？

明明逃走就好了」

「棄之不顧不符合我的性格。要是我有那種性格就好了……領民之中最有戰力我只能挺身而出。只是這樣……」

「小子你還真是個讓人佩服的傢伙啊」

「……」

魔力完全沒有了。

這種狀態下很難維持意識的。

我祈禱埃裡希哥哥他們和領民能夠平安無事的同時，當場倒下了。

6.

「嗚唔……」

「威爾，沒事吧？」

「埃裡希哥哥？」

果然，身為還不成熟的魔法使的我在松永久秀的作戰面前是贏不了。 (註:松永久秀の作戦の前に破れてしまった。指主角的戰術在久秀面前不管用的意思。根據第二節做某些語意調整)

應該爭取到非常多的時間，不過，鮑麥斯特騎士爵領情況又是如何呢？

埃裡希哥哥他們，有順利地將庫爾特的指揮權給搶過來了嗎？

清醒後不斷有擔心的事情浮上心頭，然而在看見埃裡希哥哥的時候那些擔心馬上都消失了。

「沒事吧？ 久秀先生只說你是因為魔力完全耗盡而失去意識了」

「對不起。我並沒有碰上敵軍的總大將」

「你已經很活躍了喔。久秀有說你讓他們陷入了苦戰。果然，威爾很厲害呢」

埃裡希哥哥，雖然一直在足利王國首席魔法師的名字後面加上先生的稱呼，但重要的是鮑麥斯特騎士爵領變得如何了呢？

讓我又再次擔心起來了。

「埃裡希哥哥，鮑麥斯特騎士爵領？」

「投降了。放心吧。他們並沒有做出很殘酷的事情來」

「這樣啊……」

「就算是魔法使，更還個只有六歲的威爾去成為大軍的攻擊目標，這樣的你卻還在擔心領地和領民們的事。真是讓我自身感到很慚愧」

就算埃裡希哥哥再怎麼優秀，在那種情況下能做的事應該也很少。

而且，還試著將大喊要進行魯莽抵抗的庫爾特排除了。

他已經非常努力了。

「那，庫爾特呢？」

「死了喔」

「果然……」

「嗯，是我殺的」

在正好的時機下也看見赫爾曼哥哥、保羅哥哥、赫爾姆特哥哥以及克勞斯的身影了。

他們是來探望我的嗎？

「要老老實實地投降呢，還是把指揮權轉讓給我們呢。質問過之後被他給拒絕了，所以被我砍了。唉，但是，並沒有給他最後一擊」

庫爾特，對我們下達徹底抗戰命令的同時，自己卻打算逃跑。

他想去和先前往布萊希萊德邊境伯領的父親會合，然後再打算請求布萊希萊德邊境伯派出王國軍。

因為自己是下任家主，待在父親身旁好像認為就是待在鮑麥斯特騎士爵領收復軍旁一樣。

只不過，似乎只想到要讓自己得救而已。

「他還讓他的跟班們準備了要穿越大聯合山脈時要使用的食物和裝備，還有現金等東西。我把他們都砍了。跟班是在保羅及赫爾姆特的協助下殺掉的，但是卻讓庫爾特給跑了」

他雖然身中很深的刀傷，好像還是逃進了森林裡。

「那個……在受傷的情況下逃進森林裡嗎？」

如果一邊讓血的味道飄蕩在森林裡一邊徬徨的話，是會引來狼和熊的吧。

「沒錯。死者有四人。我想已經是很溫和的解決方式了」

政變順利成功，鮑麥斯特騎士爵家諸侯軍在赫爾曼哥哥的指揮下投降了。

領地，目前處於佔領狀態。

雖然領民們很不安，不過，目前實際的損失好像都沒有的樣子。

「足利王國軍，紀律很好可以放心」

「那麼，鮑麥斯特家會如何？」

「領地被沒收了。那個國家的貴族全都是領薪水過日子的。在對方沒有犧牲而我方投降的情況下，即使鮑麥斯特家是最下層的騎士爵家他們還是同意我們存續了。目前我是臨時被任命擔任這塊領地的執政官」(註:代官，這部作品很常出現的名詞。正確是:代理官職，指替領主或大名執行領地管理之人)

足利王國，只存在給所有官員薪水的法衣貴族。

可以說是極致的中央集權性質的封建國家。

事到如今已無法逃進赫爾姆特王國領境內的鮑麥斯特家的每個人，都能在足利王國當官。

赫爾曼哥哥由於成為鮑麥斯特騎士爵家的繼承人，因此能夠以執行官的身來來管理這個元鮑麥斯特騎士爵領。

雖然實際情況看起來沒有什麼不同，但是立即性的大規模開發計畫好像開始了。

連其他的哥哥們都有官職了，如果有功績的話就能成為貴族。 (註:仕官し，這邊只要沒特別指明就是最低層的事務員或是守衛。而中央跟地方貴族的當官差別類似於軍官跟士官的陞遷制度，)

原本，足利王國的人手總是處在不足的狀態下。

因為讓小孩子來當也不是什麼困難的事，所以讓預訂離開家的哥哥們也放心了。

「埃裡希你好厲害啊。能夠直接聽到國王和王妃的聲音」

「確實呢」

「僕，指示去做投降時的談判喔」

埃裡希哥哥是帥哥，舉止也非常爽朗。

加上，在談判時也是以一副毫不膽怯磊落的態度錢去出席的。

對此很中意的國王和王妃，便直接物色他了。

「我將會去做文官的工作」

「那真是太好了」

也確認到領民們都平安無事了，就連哥哥們也將未來決定好了。

歷經了意外的糾紛，不過，也因此我也可以自由地一個人生活下去。

雖然去不了赫爾姆特王國，不過，卻可以在變成這個足利王國領地的未開發之地內當個冒險者……當我還在想著的時候，有個意外的陷阱在等著我了。

「那個啊，威爾」

「怎麼了嗎？ 埃裡希哥哥」

「陛下和王妃，有事要找威爾你」

糟了。

是讓魔法使們受傷的關係要來處罰我嗎？

「不對，因為足利王國的魔法使素質很差，不可能會放手僅只六歲就在奮戰的威爾。事情就是這樣，所以……」

僅只六歲之身就要入宮，明明才覺得好不容易才脫離前世那種社畜的日子……。

我再次被疲勞感給襲擊了。

7.

「光從遠處觀看，就覺得這孩子很了不起。對吧，今日子」

「是啊。還讓那個久秀陷入苦戰呢」

醒過來的我被足利王國的國王和王妃傳喚了，不過，起初就有各種不協調的感覺了。

在這個西洋風的世界，使用漢字的足利王國，不論是國王或是其他人也好名字都是用漢字橫式來書寫，而姓氏放在名字的前面作為表達人名的方式。

再者，在戰場上的國王和王妃，還有國王的弟弟都是將工作用的夾克披在身上。

士兵們也都有在T恤和褲子上裝備護具形式的鎧甲，還配備著合金製的刀和鐵砲。

而且，並不是火繩槍而是拿後裝填式的步槍。

似乎上面還能安裝刺刀。

也配備有類似重機關砲的東西、裝載在大板車上的小型青銅砲、用馬牽引的大砲，我想鮑麥斯特騎士爵家諸侯軍沒有打過來是正確的。

那時候我發現狙擊而來的槍聲只有一發，不過，那是由於一名士兵在急躁之下破壞了禁止射擊的命令造成的。

「久秀，把他放在赫爾姆特王國的標準來評斷的話只有中級水平而已。魔法使雖然很便利卻也是很寶貴的存在。為了彌補才裝備槍的」

我對這些人有了不協調的感覺。

這種槍和大砲，應該是不可能能夠突然間就能生產出來的。

或許，這些人跟我不是同類。

是從別的世界穿越而來，在這裡運用科學的力量來擴張勢力版圖。

「話說回來，我有問題要問溫德林君你」

今日子王妃，有事想要問我。

「現在只有我們所以我就單刀直入地問了，你是同類嗎？」

「耶多…你說這件事的意思……」

今日子小姐，似乎在懷疑我是不是從別的世界而來的居民。

但是，就算是我也不能輕易說出來。

此刻姑且先敷衍過去吧。

「我是這麼認為的。當槍擊發生時，你有說出『子彈啊！』對吧？」

為什麼會知道那件事？

唉，肯定只能先用適當的說法欺騙過去了。

「有嗎？ 我那時候正拚上性命所以不記得了。首先，王妃和我應該離有很大一段距離才對。您是聽到我的聲音嗎？」

「雖然不是親耳聽見，不過，有用望遠鏡看到了。從嘴型的動作就能理解了喔」

「……」

讀唇術嗎？

為什麼王妃會讀唇術？

「溫德林君，想要逃避大嫂的追就是不可能的喔。她以前是軍人，連反恐都很專業所以拷問也很拿手，早點坦白早點輕鬆喔」

被國王那身為宰相的弟弟清輝先生說服了，使得讓我陷入要說明來這個世界以前的人生，以及到目前為止的經歷過的過程的窘境了。

另外，在這之後清輝先生被『我到底有多兇惡啊！』而非常生氣的今日子小姐不停地踢了。

我以本能下定決心，不能忤逆今日子小姐。

8.

「誒？比我要年長？」

「現在是小孩子，就把我孩子看待就好」

「怪不得，才在想這種年紀就那麼老成了……」

「那種情況，我在網路小說中有讀過喔」

在今日子小姐的追問下，我道出了一切。

雖然也有讓人難以置信的可能性，不過，他們三人似乎很佩服似的在聽著。

「你們會相信嗎？」

「因為，我們也是從其他世界，而且還是從宇宙中被吹飛過來的」

今日子小姐，也將自己的事情說明了。

在人類可以穿梭宇宙的未來世界從事零星的運輸業，但是是連同宇宙船被吹飛到這個世界來的。

「因此，我們被迫再從這裡向南的秋津州島的近海上迫降了。那時候，正處在領主割據的時代，不過，被我們給統一了」

「這樣啊……像是稍微去買個東西的感覺嗎？」

要將島上的亂世統一起來，我認為是很辛苦的……。

「因為，我們擁有科學能力、生產力和錢喔」

一邊做生意一邊讓貨幣在統一後的秋津州島上流通起來，反抗的領主和勢力都被常備兵為主體的大軍所消滅了。

漸漸地使得有能力的領主都老實地服從了，大約過了五年就統一了整個秋津州島。

「從那之後五年一邊進行內政一邊朝隱退的目標前進，不過，再這樣下去會因人口飽和的問題得先採取行動了」

為了殖民，才會在未開發之地登陸吧。

「話說回來，你是西元時的人吧。而且，秋津州聯邦的祖先也是日本人」

雖然，現在我的外表變成了西洋人了。

「宇宙嗎……。在我的時代，如果日本人被選為宇航員可是大新聞呢」

「機器人呢？」

「還處在研究段階，不過，離實用化還很遠。産業用的機器人、ＡＩ是能夠下贏圍棋高手的水準」

「是這樣啊。在這裡的馬洛醬是機器人喔」 (註:這邊的アンドロイド/Android，指的是SF類型機器人的意思)

「哎！ 是喔？」

待在今日子小姐後面的名叫清麿的男性，根本看不出來是機器人。

看起來只是個很文靜的人。

還有，是一位非常帥的帥哥。

「看來，真的是來自和我們狠心似的世界的來訪者呢。但是，只有精神轉移到這個世界的人類身上真是罕見。讓我很感興趣了」

「對我來說，清麿先生讓我嚇了一跳……」

「這個世界，不是也有格雷姆嗎。多虧如此，就算投入了作業用的機器人不管是誰都不會覺得不可思議。因為被當成先進的格雷姆來看待了」

因為這個世界的居民，幾乎沒有人能夠理解機器人的原理。

「哎呀，能有久違的朋友談天真是太好了」

「對呀，小輝」

能坦率地交談真是太好了。

國王和王妃的心情也很好，應該就有可能會得到讓我自由生活的許可……我抱持的這樣想法真是誤會大了。

「我們不論是便利的魔法或是科學都很重視。逃去別的地方就麻煩了，溫德林君，是我們愛姫的未婚夫喔。雖然小愛大你二歲，不過，我也是比小輝年長所以不會有問題的。久秀退休後就由你來當首席魔法使，你的魔力還在成長中吧？

久秀會嚴加鍛鍊你，加油吧」

「是……」

「你(君)，太天真了。你是甩不掉我那固執的大嫂的……嗚哇！」

「那個……清輝先生你沒事吧？」

「常有的事」

今日子小姐，馬上就到清輝旁邊不在意地踢著他而在清麿先生驚訝的同時，我決心接受自己的命運了。

9.

「還真羨慕女婿你啊」(註:避免誤會提醒一下，這部分的對話是主角和松久永秀的)

「義父，我有哪一點讓您羨慕了？」

「很多方面」

「就算很多也是很不得了的喔」

「那只能當是命來接受了。我松永家，在和陛下及王妃一戰中徹底被打敗了。在滅亡的當下被撿起來了。原因在於我憑藉著有魔法的才能，還因輕視認為可以輕鬆打倒足利王國才會失去一切。唉，結果卻成為了首席魔法使，想起來人生就是如此」

從那一天起的過了六年，我今天同樣也接受了足利王國的首席魔法使松永久秀的指導了。

雖然目前我的魔力還在持續增加中，不過，非常缺乏(實戰)經驗。

除了來自師父的教導以外，足利王國的魔法使們所指導的也很有用。

只是他們魔力很少。

少歸少，卻非常擅長魔力的節約和應用。

熱衷的部分也和日本人很相似。

我也把那些拿來參考了，每天提升魔力的同時還努力進行魔法的練習。

現在的我立場是足利王家(魔法使)門生的首席，作為光輝先生和今日子小姐的長女愛姬的未婚夫，同時也是久秀的女婿。

因為他只有一個女兒，所以我也和他的女兒唯姬訂下了婚約，還會愛姬所生下的孩子創立足利分家，而決定讓唯姬所生的孩子繼承松永家。

雖然多少有些忙碌，不過，還好可以在足利王家內輕易地品嚐到日本的味道。

另外，也有做為冒險者活動著。

從十歲開始就和魔法使們進入魔之森林，用魔法將飛竜、雙足飛竜，以及其他各種各樣的魔物打倒，還在裡面採集著大量巨大的水果。

那些成果，也有被輸出到赫爾姆特王國和亞卡特神聖帝國。

起先，赫爾姆特王國有因自己的領土未開發之地被足利王家奪走而表露出憤怒。

但是，王國並沒有打算要特意派出大軍奪回未開發之地。

我想還不知道要開發到何時，不過，那也是還很久以前的事情了。

雖然目前花大錢奪回沒有意義，但是赫爾姆特王國也有身為國家的尊嚴要顧。

因為如果隨意妥協的話是會被在北方的假想敵國亞卡特神聖帝國給瞧不起，所以有秘密地在和足利王國的外交大臣本多正信在談判。

由於地國內有個名為瑞穗伯國被當成保護國來看待的國家，王國也打算模仿這個制度。

只是，王國沒辦法象帝國那樣能夠控制住足利王國。 (註:原文是王國では、ミズホ伯國ほど足利王國を押さえられない。句子內少了帝國對瑞穗伯國的控制權，整句翻起來會很怪，故做了修改)

為了請求援軍而逃到布萊希萊德邊境伯領的父親，雖然對王國政府的背叛氣到發火了，不過，不久就安靜下來了。

對王國來說，對於未開發之地在鮑麥斯特騎士爵領境內本身就有著不適合的事實。

父親在王都成為了法衣準男爵，不管改名換姓，還取了一位新的妻子。

母親在赫爾木哥哥的庇護下，兩個人形同離婚了……不，對王國來說父親是不曾有過妻子和小孩的。

未開發之地不存在於鮑麥斯特騎士爵領境內，是名為足利的貴族所擁有的領地，而他們已是王國的從屬國了。

為了把這個謊言當成事實，王國政府逼迫父親捨棄家人並且改名換姓。

拒絕的話有可能會被抹殺掉，所以父親才會接受。

爵位是準男爵，也有兼具道歉費的用途吧。

新的妻子也是一樣的情況。

就這樣就王國方面來看雖然足利家成了從屬國，不過，並沒有其他的損失。

姑且同為王國貴族是可以交易的，運用了足利家的科學能力所製造出來的商品都被大量地出口了。

特別是罐頭，最受歡迎。

因為不是每個人都會有魔法袋，所以很受歡迎。

魔之森林的水果、魔物的肉、南海的豐富魚貝類，以這些東西所做成的罐頭都賣得很好。

還有，在魔之森林中大量被採集的可可豆所做成的巧克力也很受歡迎。

只有從屬國形式的足利王國，還把這些產品銷往帝國賺了大錢。

因為王國政府的要求只能賣給帝都的商人，即使如此，帶往去(帝國)的部分由於都能高價賣出所以利潤也很高。

賺來的錢用來進行未開發之地的開發，明明還不到六年，未開發之地的中央處就已經完成了這個名為『新京』的大都市了。

大規模的治水工程、農地開發、港口和城市的整備也都在進行著，未開發之地僅在這一點時間裡就逐漸地產生了驚人的發展。

我也有大大地去幫忙了，不過，有重機具和機器人還是比較有利。

不愧是超科學的能力真的很厲害。

雖然魔法也很厲害，但就光輝陛下和今日子王妃、清輝宰相來看被當成『因為很方便所以優待了』這樣來看待。

「還有三年。不管是愛姫也好、我也好甚至連唯歲數都會增加，只要女婿一成年馬上就能有孩子了吧。這樣，我松永家也能安泰了」(註:這時候的主角是12歲，15歲是成年)

松永久秀，已經用女婿來稱呼我了。

因為我是他女兒唯姬的丈夫，同時也是他也是我的魔法師父。

在魔力方面我具有壓倒性的優勢，但是在技術方面他則是一位值得尊敬的魔法使。

其他，我也接受了來自島清興、堀秀政、武藤喜兵衛、黒田官兵衛、竹中半兵衛、片倉小十郎、太原雪齋等人的指導，他們各自的擅長的魔法都不同且各有優點。

雖然他們名字和戰國時代的人物一樣，不過，不需要想太多。

另外有一件很困擾的，就是大家都提過要把自己的女兒嫁給我。

因為曾被今日子小姐『拒絕不了的喔。大家都想要你來當他們的女婿呢』這麼說過了，所以將來肯定會是這樣吧。

說起來，現在的我名字叫過足利溫德林。

這樣混搭的名字雖然很奇怪，不過，我覺得不需要去在意。

「我們足利王國，雖然能使用科學的兵器和訓練精良的常備兵來對抗琳蓋亞大陸的兩個大國，但不曉得持有上級魔力的魔法使到底有多可怕。女婿，也花了好幾年的時間學會了我們幾十年反覆辛苦才學會的大部分魔法了」

此外，認為這兩個國家各自擁有幾十名魔力較多的上級魔法使。

現在我們就算人少也要努力追上，因此也就有必要強化魔法使。

但是，現在在鍛鍊我的同時好像也只有我這個新人被期待而已。

「不知道和平能夠持續到什麼時候」

國與國之間不會有真正的朋友，整頓好軍備的赫爾姆特王國也是有可能會攻來未開發之地的。

面對目前這個地方的開發推進的情況和靠獨自的交易品所形成財富的足利王國，不知道什麼時候赫爾姆特王國是有可能會前來拔除獠牙的。

「一對一的話，不認為兩國頂尖層級的魔法使會輸」

「假如是一對多就很嚴峻了，義父是最清楚的吧？」

「是啊。六年前就是靠這樣才贏女婿你的」

足利王國的魔法使，平均的魔量很少。

因此是以技術和合作來彌補，在這個領域比那二個大國還要強說不定可打得贏。

據說在統一前比較偏向於單人技術，但是今日子小姐已將魔法使有關戰鬥方法和合作的部分集結成教案了。

不愧是原高級軍人。

因為還是一位醫生我覺得很厲害。

「雖然能看見兩國都想避免戰鬥，但問題是能持續到什麼時候」

「因此，準備是有必要的……「不好了！

尊師大人！ 溫德林大人！」」

突然，有一位士兵飛奔到我們所在之處了。

似乎發生了某種緊急事態。

「什麼事？」

「那個，赫爾姆特王國的王都毀滅了！」

「「蛤？」」

我和義父，一開始都對那件事實感到不可置信。

在沒有核武的這個世界，是如何將一座人口百萬的大都市給毀滅掉的。

「是竜！ 傳說中説的古代竜做的！」 (註:再次強調竜、龍、ドラゴン三種是不同種類的龍。等同於エルフ會改譯成森精的道理)

「這樣啊……女婿，我們走」

「是」

我們結束修行，前往在新京中央處的王城。

足利王國的王城，以外表向臣民們展現力量，在戰時也能建造成要塞來使用。

我想如果要認真建設的話也得花上好幾年，不過，靠著驅使機械的力量一年就開發好了。

同時心經也大規模被建造起來，因為上下水道完備，為了將來能讓電力普及，也有預先做好地下電路專用的管線空間。

秋津州島的首都機能也轉移結束了，雖然新京的人口約有二十萬不過看起來卻比兩大國的還要進步。

接受完守護王城的士兵們的檢查後進入稻城內的作戰室時，在巨大的螢光幕上顯現出化成瓦礫的赫爾姆特王國首都那副廢墟般的模樣。

「這麼殘酷啊……」

來自無人偵察機的偵察，鮮明地顯示出化為瓦礫山的王都景象。

仔細一看還能看到屍體四處散落著，所以讓我儘量不想去看見。

「傳說的古代竜。我還認為只有在故事裡才有。久秀你知道哪些呢？」

「活了數萬年的時間，連屬性竜也成為不了對手的強大之竜。說不定只有過去的人，才有見過實物」

面對光輝陛下的疑問，義父簡潔地回答了。

「數萬年一次的大災害啊。讓我感到不可思議的是，赫爾姆特王國不是有許多優秀的魔法使……」

靠無人偵察機是有限的，足利王國為求更詳細的情報在等待密探的報告。

另外，統合足利王國諜報部門的，是名為擺第三太夫的人物。

好像在哪邊有聽過。

幾天後，詳細的報告傳來了。

足利王國的諜報能力似乎非常高。

大概是受軍人的今日子小姐的薰陶吧。

嘛，無人偵察機，是科學的力量吧。

不僅是依靠機械力，我想甚至連諜報組織的強化部分都沒有間隙。

我能成為足利王國的一族真是太好了。

「赫爾姆特王國，即使投入了被稱呼為最終兵器的阿姆斯特朗導師，以及底下大量的魔法使，但由於古代竜是不死者，所以首戰以單方面的犧牲敗北了。沒有退路的赫爾姆特王國，請求教會派遣會使用『聖光』神官們。教會接受這件事情後，除了進行王都居民避難外也一起進行了迎撃作戰，但還是敗北了。雖然赫爾姆特三十七世以下的王族都成功逃離了，但是阿姆斯特朗導師卻戰死，而魔法使也大量損失了。神官方面也出現很大量的犠牲。王都毀滅後，死傷者將近五十萬人。沒救了」

「是啊。連一國的首都都能毀滅的東西……」

王都化成了瓦礫。

不只是一座大都市的崩壞。

赫爾姆特王國是一個中央之力很強的國家，而那股力量的源頭毀滅了。(註:作者的形容法很瞎，其實寫成中央集權就好，而力量的泉源就是王宮內的魔法使)

連支撐中央集權施政的政治家和官僚都受害很嚴重，王國軍方面就更不用說了。

在居民死傷近半數的時間點來看，赫爾姆特王國等同是滅亡了。

「陛下、王妃。不做點什麼嗎？」

「這樣看那頭不死的古代竜的動向決定了。話說回來，那位王國的最終兵器戰死……」

「沒錯。雖然聽說他具有如同怪物一般的實力……」

「雖然也能使用聖屬性的魔法，但威力很差……教會的神官們，不擅長戰鬥。以阿姆斯特朗導師以下的魔法使來阻止古代竜的行動，趁這時候神官們則用『聖光』給予關鍵性的一擊。但是，作戰卻是失敗了」

義父的推測事後得到了判明。

很清楚自己的魔力比誰都還要來的少的義父，絲毫不懈怠地在收集著兩大國的魔法使相關的情報，幾乎能成功又正確地分析出他們的戰鬥力。

「那麼，古代竜的動向是？」

「在大鬧一場」

據說古代竜在王都周邊有一定人數居住的地方進行著無差別式的攻擊。

好像連足利王國所派出的密探們都已經有出現犧牲者了。

「西部、東部、南部。有三名有實力的邊境伯。中央和北部，化成了古代竜解悶的場所。能否控制住他們的野心是不明的」

任職於足利王國外務大臣的本多正信，很清楚兩國的貴族。

他，推測三名邊境伯不會積極去擴大勢力的。

「不敢靠近中央嗎？」

「因為有古代竜在啊」

「比起王國的貴族，北方更還有假想敵國在，而他們會怎麼做呢？」

我不由得發問了。

雖然兩國處於停戰中，但那是有巨大的斷層在，國力才能保持平分秋色。

既然赫爾姆特王國已經虛弱了，說不定會赤裸裸地露出野心來。

或許戰亂的時代要開始了。

「持有帝國南方領地的選帝侯紐倫堡公爵，似乎幹勁十足呢。以對諸侯軍下達動員了。帝國軍則還沒有動作」

「那也就是說，紐倫堡公爵的野心暴露出來囉？」

「很有可能」

對於我的問題，本多外務大臣回答了。

「對紐倫堡公爵來說，說不定是想先奪取混亂之下的王國領土來蓄積力量，之後也打算進攻帝國吧」

一頭古代竜，就讓大陸陷入戰亂了。

「該怎麼做才好呢？」

「巧婦難為無米之炊。現在還是先整備未開發之地吧。如果大聯合山脈以北的布萊希萊德邊境伯向我們這邊露出野心的話，我認為我們應該要將國力積蓄起來」

「我賛成」

「同樣」

不論是義父、本多外務大臣、軍方，都反對要去侵略赫爾姆特王國。

不管怎麼確保領地，之後要是被古代竜襲擊的話就沒意義了。

「不預先做好對古代竜迎撃準備是不行的。會以人們聚集起來的地方為目標吧？」

「是的，王妃。古代竜化成不死者了。因為是死的存在，說不定會憎惡生物」

「像是『明明我死了，你們卻還活著！』的感覺？」

「嗯，因為不死者擁有一切的感情。所以才會襲擊生者吧」

「那麼，要在大聯合山脈設置光束砲嗎？

讓神奈川修理那傢伙」

光束砲……。

這些人，真的是超未來的人類啊。

不認為使用火藥的火器能對古代竜行得通，但用光束砲會有勝算嗎？

「那樣子，也要先將大聯合大山脈的飛竜和雙足飛竜給驅逐掉吧。沒有的話，可是無法派遣軍隊的喔。所以，請多多指教了。我可愛的義子君」

「是……」

比接受義父他們，或是軍方將校他們的嚴格訓練要來的好一些吧。

那麼想的我們，應該先在大聯合山脈上建設起防衛古代竜的基地，而展開了驅逐飛竜和雙足飛竜的作戰。

「光是靠我們一天能討伐的竜數量是有限制的。有女婿在真是幫大忙了」

從那一天起，我日復一日地窩在大聯合山脈裡持續在消滅竜。

直到全滅為止，我的工作是不會結束的。

大聯合山脈也被視為是魔物領域，意外的是這裡並沒有首領。

因此，如果不全部消滅掉的話是無法解放的。

連在這段期間，赫爾姆特王國的中部和北部都隨古代竜的腳步被徹底摧毀了。

而且，也傳來利用那狀況朝王國境內侵略而來的紐倫堡公爵受到報應了。

古代竜的下個目標選在紐倫堡公爵領，而且諸侯軍毀滅，連紐倫堡公爵本身都行蹤不明。

首先，他應該不可能還活著。

古代竜，一個月左右就迫使帝國毀滅了。

許多貴族領和其大軍都毀滅了，多數的魔法使也出現了死傷，各地的基礎設施和治安也都被徹底破壞掉了。

以為破壞帝國就能滿足之時，這次卻有報告傳來牠的再次南下是以還存活的王國領為目標。

如果赫爾姆特王國毀滅的話，接下來肯定就輪到足利王國領了吧。

「真實的怪獸電影啊……」

「會移動的災害還真是困擾呢」

今天也同樣在結束完討伐竜的工作而回到聯合山脈山頂上的建設中的砲擊基地時，在那裡我和在做光束砲的修理和調整的清輝宰相交談。

因為他知道我的秘密，所以也有為此準備了話題的素材。

「宰相閣下，這門光束砲能贏古代竜嗎？」

「比起上級魔法使笨拙的魔法，更有能讓牠停步的威力。雖然做了各種的調查，果然不死者好像不用『聖』屬性的魔法是無法打倒的。用光束的火力就能打倒的話，上級魔法使的火力也應該能給予損傷才對。哎呀，科學力什麼的好像不適用於幻想呢」

「誒？ 那該怎麼辦才好？」

「有計算過，應該能讓牠有相當長的一段時間無法行動。因此，靠我的義甥殿的『聖光』給牠致命的一擊就行了。因為你的魔力非比尋常，基本上是能完成任務的」

果然，只能用從師父那學到的『聖』魔法做點什麼了……。

「問題是要如何讓古代竜接近這座砲台……」

「沒問題。古代竜，對人工建築物也有反應。所以，兩國的魔導飛行船幾乎沒有平安無事的」

用魔力來驅動過去遺產的魔導飛行船，這些大部分都成為古代竜的目標了。

中型以上幾乎沒有平安無事的，被徹底破壞掉的船想要重建是不可能的。

動力來源的巨大魔晶石也被打碎了，以目前的技術是不能製作出來的。

足利王國雖然也有許多類似的船被建造出來運用了，不過，這是都是以反重力裝置和電池來驅動的。

雖是由足利王國所生產……但幾乎是由這位清輝宰相所獨佔。

反正就算外人知道結構，一開始也沒辦法模仿的吧。

「我們的光束砲照預定計畫，可能會使難民的數量增加吧」

特別是，來自帝國的難民。

王國的難民，因為有大聯合山脈在所以幾乎不會前來。

因為帝國幾乎毀滅了，所以會陸續搭船湧來。

特別是，很多都是來自持有帝國北部領地瑞穗伯國的居民。

他們原本都是同一個民族，作為難民應該很容易被接受。

其他的難民也有很多人在發起騷動，但足利王國光是要接受他們就要竭盡全力了。

多虧了科學力，據說和其他存活的赫爾姆特王國領相比在紛爭上要壓倒性來的少。

布洛瓦邊境伯領、霍爾米亞邊境伯領、布萊希萊德邊境伯領等，都發生了武裝難民和諸侯軍的衝突。

活下來的難民樹比起減少下來的食物產量還要多，也造成了食物的搶奪。

「比起大聯合山脈北方已是地獄了呢。發揮同情心可是會讓我方自取滅亡的。雖然很可憐，但是不能去幫忙。如果來到足利王國，不是犯罪者就會給予保護，不過，若是犯罪者呢？即刻會讓他們當硝石山的材料」

雖然說得很可怕，不過，清輝宰相所言之事是不會有錯的。

現階段，比起大聯合山脈往北走要更危險。

10.

「布洛瓦邊境伯領、霍爾米亞邊境伯領、布萊希萊德邊境伯領。所有領主館位處的所在地都毀滅掉了。把那些地方全都破壞完的傢伙，果然目標是我們這裡」

兩國，終於是被古代竜毀滅了。

都市、基礎設施、大規模的農地等都被徹底地破壞掉，活下來的人們也圍繞在食物和居住場所的爭奪上。

琳蓋亞大陸，確實已化成世界末日般的狀態了。

古代竜即使做了這些還不滿足，終於把目標放在跨越大聯合山脈才能到的足利王國的破壞上。

「真的朝這裡而來啊ーーー！」

「誒？ 難道你是認為不會過來嗎？」

「溫德林殿，常務總是給人這樣的感覺」

在山頂上的砲臺基地內，除了清輝宰相和清麿先生外就幾乎沒有人了。

被古代竜當成目標的可能性很高，光束砲本身只要清輝宰相和清麿先生在就足以運作起來了。

話說回來，每次看到清麿先生只覺得他就是這人類。

還有，清輝宰相原來是常務啊。

和普通職員的我不同感覺好厲害。

「我的臉上有什麼東西嗎？」

「沒有，在我的時代機器人的水平比較低」

「在那個時代即是如此也是沒辦法的事。好，發射準備完成了。自動追蹤裝置也正常」

「好快！」

我，『能源充填率百分之○○！』在朗讀和對話的同時還在隨意地想著事情。

「因為那樣子太浪費時間了」(註:指等待填充的時間)

「我的義甥殿也是這麼認為的吧？

清麿是不是幻想呢」

「因為我是機器人。溫德林殿也請你準備一下」

「瞭解」

戰術很單純。

根據雷達和無人偵察機，古代竜似乎朝這裡接近過來了。

來到這裡時會用光束進行射擊使其停止動作，這段期間我則是用『聖光』來殺掉牠。

失敗的話就糟糕了，所以漸漸感覺壓力上來了。

「靠社長和副社長，第二、第三發也做好準備了。請不用太緊張」

原來如此。

就像某宇宙戰艦所出現的技術負責人一樣，『還有這種事情啊！』在這樣的感覺下還做了其他的準備。

是這樣的話就放心了，於是讓我放下心來嘆口氣了。

「那麼，發射！」

清輝宰相按下按鈕後，從山頂上所設置的光束砲台有一道粉紅色的光束被發射出去了。

簡直就像○彈一樣。

「很驚人的威力……」

「因為是商船做自我防衛用的東西，和軍艦的砲台相比就像是小便一樣」(註:如果不是作者腦子有裝大便的話，跟小便一樣細應該叫光束射線才對)


就如清麿先生說的那樣，挨上一記被發射出去的光束砲的古代竜動作停下來了，還不斷有煙從體內冒出來。

明明有這樣的威力，卻打不倒不死古代竜我覺得還真是嚇人。

即使光束的照射結束了，古代竜還就那麼浮在定點身上也持續在冒著煙。

與其說沒受到傷還，還不說用聖屬性以外的攻擊所受到的傷害都開始在恢復起來的樣子。

在完全恢復之前是動不了的，也就是說該是輪到我出場了。

慎重地，用『飛翔』接近不斷在冒煙的古代竜。

當然為了慎重起見有展開了堅固的『魔法障壁』了。

即使我接近了，古代竜為了恢復傷害還是處在當場動彈不得的狀態。

「原來如此，撿現成的呢」

即使如此也不能掉以輕心，即刻就將強力的『聖光』展開來了。

古代竜似乎還是無法動彈，反而身體被烤到熱起來像是發出巨大的悲鳴一樣在徬徨著。(註:句子就是這麼寫，其實很想改成掙扎)

在一分鐘左右的『聖光』照射下會持續烤著古代竜吧？

終於，只剩骨頭的古代竜四分五裂崩落下來了。

我急忙將古代竜骨頭和巨大的魔晶石給回収了。

「成功啦ーーー！ 就像是小說裡的主人公一樣！」

「什麼？ 那是？」

雖然莫名地被清輝宰相稱讚了，不過，我們終於把琳蓋亞大陸的災厄不死古代竜成功消滅了。

11.

「即使打倒古代竜，除了足利王國領以外的琳蓋亞大陸都還處在混亂之中。兩國的王族和皇族也幾乎都全死了。難民太多，再這樣下去土地會不夠。貫穿大聯合山脈的隧道已經發現了，而且電磁飛行船的量産也在進行當中。船員，則可以想辦法僱用逃出來的舊赫爾姆特王國的空軍軍人來教育新人就好」

「那麼，就由威爾軍來率領先鋒部隊吧」

「好殘忍。我還只有十五歲……」

「可是，你(君)如果加上原本的年齡，就已經超過三十了不是嗎。啊，也把愛姫一起帶去吧。還有，你(君)的老婆們喔」

「是……」

打倒古代竜後過了二年半，我迎娶了光輝陛下和今日子王妃的長女愛姬為正妻，其他還有松永久秀唯一的女兒唯姬，然後連其他王國內魔法使的女兒也都成為我的老婆了。

雖然被清輝宰相說『這不是在開後宮嗎！

何等小說般地展開！』了，不過，都被大家以經常以來的毛病發作了無視掉了。

然後，因為結婚所伴隨而來的性行為讓太太們的魔力都提升了。

魔法使們都欣喜若狂，使得我太太不斷增加中簡直就像一支軍團一樣。

那樣子的我們，終於要跨越大聯合山脈出徵去。

不過是生還下來的貴族們彼此間小摩擦般的程度，說不定還有難民之間的爭吵，不過，至少足利王國如果不將將舊赫爾姆特王國合併起來進行開發的話有可能會因為難民的關係而出現自取滅亡的下場來看，就只能勉強進行合併了。

已經是足利一族的我，不知為何要和未來人們的女兒變得可以使用魔法的愛姬一起出徵了。

當然，其他二十二名側室們也會一起去。

話又說回來，我有這麼多老婆感覺還真是奇怪。

但是，我又拒絕不了。

已經對我的特殊性判明後，增加魔法使一事對足利王國來說成了當務之急。

愛姬的孩子會成為足立分家的下任家主，而其他老婆的孩子也會繼承父母家的家督。(註:跟家主的意思一樣...純粹作者賣弄文學)

因為魔法使全都是貴族，所以後嗣好像會成為足利分家血統的繼承人。

其實，我已經有生一名男孩子了。

結婚前『你應該學學女人！』而被義父趕鴨子上架了。 (註:義父上に押しつけられたのだ。老實說作者這麼寫是主角當受被久秀給搞基了...真正受詞是指唯姬)

孩子是出生了，不過，那個孩子突然之間魔力卻變多了。

因為在魔法使的體質不受遺傳這個常識影響下(孩子居然會有魔力)，讓義父（松永久秀）高興到流下眼淚，而催促我要快點和唯姬生孩子。

因此，我的太太們全都不會從我身旁離開。

因為自己也成為魔法使所以才會陪伴我，沒懷孕是不能回到後方的。(註:同樣是作者語意的問題，這邊是出現懷孕而要安胎不能繼續奔波打仗，並不是要離婚的意思)

「哭泣……」

「嚇我一大跳啊……阿爾弗雷德的弟子是後宮王啊」

最初進攻的地點是舊布萊希萊德邊境伯領，我和師父的師父相遇了。

這位名叫布朗塔庫先生的人，他率領布萊希萊德邊境伯領還生還的家人與難民像我們投降了。

布萊希萊德邊境伯本人，即使要讓多一位居民從布萊希堡逃走，而成為古代竜的食物了。

似乎是一位相當了不起的人物。

「我的工作到此結束了。之後……會在足利王國度過餘生」

布朗塔庫先生拒絕了光輝陛下和今日子王妃的邀請，偶爾會用在魔之森林裡狩獵所賺來的錢來過舒適的生活。

因為辛苦到現在了，似乎沒有多餘力氣可以再為官了。

古代竜死後的琳蓋亞大陸，我們不斷地北上和各式各樣的人相遇了。

然後……。

「由於我的不中用，而害死了家人和伯父，以及很多的人。雖然想盡辦法從毀滅掉的王都逃了出來，卻只能救到這些人而已」

從王都逃出來的教會有力人士的孫女是一位有著驚人尺寸般巨乳的神官小姐。

「威爾君，你在看哪邊啊？」

「愛，這是男人的本能」

她說，如果我們足利王國軍如果能平安保護她所帶來的難民，就願意與我們同行。

因為是她是一位能使用治癒魔法的人，我認為她是必要的戰力。

「大家，都是溫德林大人的夫人嗎。……我明白了」

不知道到底明白了什麼，不知為何我不斷與女孩們相遇了。

「希望你可以僱用僕和伊娜醬。因為，我們對自己的本事很有自信」

「如果不僱用，今天連要吃飯都會成為問題」

「我叫維爾瑪，姓氏捨去了。對力量很有自信」

「如果能讓維爾肯領的領民們有安全的場所居住的話，我願意成為溫德林先生的力量。即使是這樣我，也是一位持永『暴風』稱號的魔法使。雖然不敵古代竜……」

就這樣，越來越多就算不是成為夥伴而是成為老婆的人增加了，就這麼持續到足利王國統一大陸為止。
