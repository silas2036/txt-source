走到大街上後，米夏問道：

「要用魔法模型做什麼？」
「啊啊。」

我將剛才做好的魔法模型放在指尖上，遞給米夏。

「難得第一次做了魔法模型，所以想讓米夏看一下呢。」

米夏直眨著眼，然後開心地微笑起來。

「謝謝你。」

她用魔眼直直凝視起魔法模型。

「怎樣？」
「⋯⋯好厲害⋯⋯」

米夏宛如注視般的細細品味，改變角度，從各個方向觀看我做的魔法模型。
我愣愣望著她的這副模樣，即使過了好幾分鐘，她也依舊沒有要別開目光的意思。
這還是第一次看到米夏這麼沉迷的樣子。看來不光是製作，她對魔法模型本身也很感興趣呢。

「漂亮。」
「是嗎？」

米夏點了點頭。

「就連看不到的地方，都好好地做出來。」

被發現了嗎？真不愧是米夏呢。

「因為『創造建築』的重點在於內部呢。如果是創造劍的話，要是沒考慮到內部會是怎樣的構造，就無法具備正常的強度。雖說魔法模型只需要給人觀賞就好，但就算只讓外表相似，也不會形成相同的模樣。」米夏忙不迭地點頭，認真聽著。

「在創造石頭時，不要創造石頭，而是要創造構成石頭的原子──這在神話時代可是耳熟能詳的論述。」
「誰說的？」
「我說的。」

只不過，這是說來容易做來難，因此實際上能做到的人並不多。

「⋯⋯⋯⋯」

米夏再次直盯著魔法模型。

「這麼中意的話，就送你吧。」

她稍微瞠圓了眼。

「可以嗎？」
「是你今天陪我出遊的謝禮。」

我施展「創造建築」的魔法，用那個魔法模型代替寶石，製造出戒指，然後套在米夏的右手食指上。

「這樣就能在想看的時候看了吧。雖然是個一點也不亮眼的枯燥戒指。」

米夏忙不迭地搖頭，露出拘謹但十分高興的笑容。

「這是最漂亮的。」
「是嗎？」

米夏點了點頭。

「⋯⋯阿諾斯什麼都辦得到⋯⋯」

米夏看著魔法模型的戒指，以幾乎是喃喃自語的感覺脫口說道。

「還好啦，也沒什麼做不到的事呢。」

語罷，米夏就有點沮喪地說道：

「⋯⋯我什麼都辦不到⋯⋯」
「沒有這種事吧？」

米夏朝我看來。

「阿諾斯幫助了我。」
「是呀。」
「所以，我想要回報。」

沉默了半晌，米夏接著說道：

「阿伯斯・迪魯黑比亞是冒充者。我也想助阿諾斯一臂之力。」

還真是說了句值得讚賞的話呢。

「可是，什麼都辦得到的阿諾斯，不需要我。」

原來如此。是因為這樣才沮喪的嗎？米夏還是一樣很溫柔呢。

「這可不一定。」

米夏直眨著眼。

「你有雙好魔眼，也很擅長創造魔法。如果只限於這兩方面，說不定能超越我喔。」
「⋯⋯真的？」
「即使是我，也絕非萬能。今後也無法保證不會出現我從未想過的不可能事物呢。要說的話，我能勝過這世上所有人的，就只有毀滅什麼的力量。」

毀滅、毀滅、毀滅一切，將不可能化為可能。不過，我也沒有愚蠢到會自視過高地認為：今後不論發生什麼事，自己都能做到相同的成果。
準備是愈多愈好吧。

「而你的創造魔法則是跟我截然相反，說不定總有一天會派上用場。」

當然，要辦到這點，米夏的成長是不可或缺的吧。

「如果想助我一臂之力的話，就要更加逼近魔法的深淵。」

就像下定決心般，米夏點了點頭。

「等我。」

眼中帶有堅定的意志。

「我儘是受到阿諾斯的幫助。可是，總有一天會回報的。」
「我很期待喔。」

這時，傳來「喵──」的鳴叫聲。
一隻黑貓從某棟建築物的窗戶探出頭來，招牌上寫著貓咪咖啡廳「木天蓼亭」

「⋯⋯喵、喵⋯⋯」

隨後，米夏模仿著貓叫聲，呼喚著黑貓。只不過，黑貓從窗外縮回屋內。

「⋯⋯喵⋯⋯」

米夏大為失望。

「要進去嗎？」
「⋯⋯可以嗎？」
「這裡是目的地。」
「⋯⋯阿諾斯也喜歡貓⋯⋯？」
「差不多吧。」

走進木天蓼亭後，充滿精神的招呼聲「歡迎光臨」響起。
店內有好幾隻貓走來走去，米夏「喵、喵──」地頻頻叫喚著。就座後，一隻白貓就靠了過來，坐在她的膝蓋上。

「阿諾斯，你看。」

米夏開心地說道。

「好可愛。」
「太好了呢。」

她帶著笑臉點頭。

「喵？喵──？」

米夏一面好乖、好乖地摸著白貓的頭，一面模仿著貓叫聲向它搭話。當然，貓是不可能回答的，它就只是在米夏的膝蓋上放鬆休息。
隨便點了紅茶後，一隻黑貓跳到我背後的櫃子上。是方才把頭伸出窗外的貓。

「辛苦你了，艾維斯。」

米夏驚訝地看著黑貓。隨後，黑貓就開口說話。

「還請原諒小的以這身姿態污了陛下的眼。」
「無妨。」

畢竟不能讓人察覺到艾維斯還活著呢。
不是經由魔法聯繫，而是像這樣直接會面，也是為了要避免引起阿伯斯・迪魯黑比亞的注意。
昨天向鎮上派出貓頭鷹，是我想跟他見面的訊號。在確認到訊號後，艾維斯就會主動找到我，進行接觸。這是之前用「意念通訊」交付記憶時，順便訂好的規則。
因此，今天才會像這樣請米夏陪我出門，在鎮上閑晃到艾維斯與我接觸為止。

「查到什麼了嗎？」
「關於七魔皇老之一的梅魯黑斯・博藍，有件無法理解之事。他雖是屬於統一派，卻不是統一派的首領。」

唔，這樣確實是很奇怪。就算說是統一派，幾乎是最具權力的七魔皇老居然不是首領。

「那麼，統一派的首領是誰？」
「儘管調查過了，但連我也無法掌握其身份。統一派的首領決不會公開現身，不僅如此，似乎連在統一派之中，也無人知曉其真實身份。」
「就連梅魯黑斯也是？」
「沒錯。」

這又是一件極其可疑的事呢。

「嗯，如果是當今魔皇之中的某人，這也不是無法理解吧？自己是統一派首領的事假如泄露出去，就很有可能失去魔皇的地位。」

七魔皇老不論如何都會是七魔皇老吧，但魔皇就有被替換的可能性。

「不過，要是連你都無法掌握身份，說不定會是活過神話時代的魔族呢。」

也能認為那傢伙就是阿伯斯・迪魯黑比亞。不過要是這樣的話，他當上統一派的首領，是有什麼企圖嗎？是在控制統一派與皇族派的勢力平衡嗎？

「我這邊也清楚了一件事。梅魯黑斯失去了我的記憶。而且，根源只有一個。」
「接觸過了嗎？」
「沒錯。他是親自見到我的根源後，注意到我就是暴虐魔王的樣子。儘管是自己人的可能性很高，但我沒說出你的事情。」

艾維斯動也不動地等候著我的命令。

「去調查梅魯黑斯，還有統一派的首領。儘管姑且清查過記憶了，但就只有表層。而且說不定是用與根源融合不同的方式讓我讀不到記憶。」
「遵命。」
「其他還有查到什麼嗎？」
「有一件。是說不定跟那個統一派首領有關的情報。」

店員剛好把點的紅茶送來。艾維斯暫時緘默下來，等她離開之後，才再度開口說道：

「這座城鎮有間叫做羅古諾斯的魔法醫院，是魔皇艾里奧使用私費建設的醫院，有著能受到迪魯海德最好醫療的評價。不過，魔皇艾里奧就只是個傀儡。他的背後似乎還有其他魔族在⋯⋯」
「就算調查，也掌握不到真實身份嗎？」

艾維斯頷首。

「也能認為此人跟統一派的首領是同一個人物吧。」
「我知道了。其他還有什麼事嗎？」
「是還有幾件事，但還不是確定的情報。」
「那麼，最後一件事。去調查魔劍大會，特別是七魔皇老有沒有參與其中。」
「遵命。」

艾維斯自窗戶離開。
不過，身份不明的魔族嗎⋯⋯擔任統一派的首領還可以理解，但建設魔法醫院的目的是什麼？當中有什麼關聯性嗎？還是說，雙方是不同人嗎？

儘管還不清楚，但就稍微到現場看一下吧。

───

喝了紅茶，稍作休息後，請米夏帶路來到羅古諾斯魔法醫院。

「這裡。」
「唔，看來是棟很大的建築物呢。」
「有很多住院患者。」

似乎沒有特別可疑的地方呢。儘管用魔眼大略探查過建築物，但只感受到微弱的魔力。

「阿諾斯。」

米夏用手指著。雷伊在那個方向上，剛好從魔法醫院出來的樣子。

「早。」

走過去向他打招呼後，雷伊就轉過身來。

「咦？阿諾斯？你怎麼了嗎？」
「只是經過而已。你才是怎麼了，不會是感冒了吧？」

語罷，雷伊就像傷腦筋似的微笑起來。

「就稍微來探望一下母親。」

也就是說住院了吧。

「身體不好嗎？」
「身體天生有點虛弱呢。不用擔心啦。」

他雖是這麼說，表情卻很苦悶呢。

「如果醫生治不好的話，我會設法解決的。」
「哦？阿諾斯也很擅長治癒魔法啊？」
「沒什麼，還算不上是擅長，頂多就是讓瀕死的重病患者，明天能健康到當日征服尼爾山脈歸來吧。」

雷伊吟吟微笑。

「有點健康過頭了呢。」
「所謂真正的醫療魔法，是要讓人比生病之前還要健康喔。」
「感覺怪可怕的，我就心領了吧。」

唔，看來不是很嚴重的病呢。

「啊，對了。先跟你說一聲，我可能不會參加魔劍大會。」

霎時間，雷伊的表情黯淡下來。不過，立刻就恢復笑臉。

「這樣啊。那麼，到時候就等下次再一較高下吧。」
「你不問理由嗎？」
「咦？啊啊⋯⋯是怎麼了嗎？」
「沒有理由。」

雷伊一副出乎意料的模樣。

「⋯⋯我想就依阿諾斯想做的去做就好。」
「還以為你會為了分出勝負而要我出賽呢？」
「我不喜歡強迫他人。」

唔，好吧，是很像他的個性。

「那麼，學院見。」

稍微抬起手這麼說後，雷伊就離開了。

「你覺得呢？」

經我詢問後，米夏說道：

「⋯⋯跟平時有點不同。」
「我看起來也是這樣呢。」

就像是感到內疚的態度，是怎麼了嗎？平時的話，我是不會太過在意，但畢竟這裡是這種場所。為了小心起見，就派艾維斯去調查吧。