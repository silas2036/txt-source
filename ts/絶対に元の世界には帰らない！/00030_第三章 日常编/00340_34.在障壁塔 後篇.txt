明明有股寒氣直襲心中，身體卻越來越火熱。

「啊、咕⋯⋯！」
「影、影耶⋯⋯？！」
「唔⋯⋯？哈哈，魅惑的效果終於出來了嗎！」

漸漸地，身體的火熱侵蝕到了心中。

「過來，女人」

內心一震。普拉特聲音溫柔地回響在我耳邊。

雖然內心叫著不能過去，但被熱氣侵蝕的身體，自然而然地向著普拉特靠近。

「普拉特⋯⋯」
「給我加上大人，無理之徒」
「對、對不起，普拉特大人」

我立刻低下頭。眼角不禁熱了起來，只是想像被這個人討厭而已，我就要哭出來了。

普拉特大人打量似地仔細掃視我的身體。就連這纏上身上的視線都讓我覺得滿足。

「嗯呣⋯⋯不錯。相當有魅力呢，之前的不敬就算了吧」
「魅、力⋯⋯？欸、欸嘿嘿⋯⋯」

太過開心，不小心就笑了出來。被他稱讚，幸福的心情不可思議不斷涌出。

「我可愛嗎，普拉特大人⋯⋯？」
「只有強大的力量能讓我感到魅力，我對你的容姿一點興趣都沒有。」
「你說什麼啊該死的傢伙！」
「咕噗！？」

我全力賞了普拉特腹部一個膝擊。

什麼不說，竟然敢說對我家的完美美少女影耶沒興趣？

這可是我和夏緹亞費盡心思製作，無論男女老少都睜大眼睛的最最可愛的女孩子，用那麼下流的視線盯了這麼久的感想竟然就是這個什麼的，我絶不允──

──這時，我的腦中又閃過一道紅色閃光。

「咕，這女人⋯⋯！」
「⋯⋯普拉特大人⋯⋯」
「哼，我可不會再被騙了！竟敢小看本大人！」
「對、對不起！拜託了，請原諒我，不要討厭我⋯⋯！」
「⋯⋯難道是效果還不夠徹底嗎？《魅惑的燐光》」

紅色閃光從普拉特大人手中放出。他手的動作如此性感，讓我不禁小鹿亂撞。

光芒刺上我的身體。總感覺有點癢。

「恩⋯⋯欸嘿嘿」
「好了⋯⋯這樣應該就沒問題了吧」

雖然完全沒效，但這說了肯定又會讓普拉特大人不高興，還是先不說了吧。

「好，過來吧。⋯⋯不對，以防萬一武器就放在原地」
「好的」

我開心地把刀丟在地上，向著普拉特大人跑過去。

然而，確有什麼東西拉住了我的身體。

「放開我，伊緹⋯⋯」
「不、不行啊影耶！請你清醒過來！」

淚目的伊緹這樣對我說道。

不過，就算這樣做我的心還是普拉──等等，淚目的伊緹是不是有點可愛過頭了啊？
平時那麼無表情，給人一種工作無所不能的感覺，現在卻擺出這表情不是太犯規了吧。

沒錯，就是這樣。我所追求的就是這種知性與可愛的反差。像夏緹亞那種只有外表冰山美人，內在卻是個笨蛋的就容我婉拒。
那傢伙之前好像還搞錯了什麼，耍蠢用菲露斯把房間降到零下一百度說什麼「哼⋯⋯我，超冷酷」之類的白痴。這傢伙是笨蛋嗎。

「（奇怪，我剛才是在想什麼來著？）」

想要回想起數秒之前自己的瞬間──紅色閃光閃過。

普拉特大人拉著我的手腕，就這樣把我拉到他的身邊。

「你在幹什麼，快給我過來」
「啊唔⋯⋯」

謝罪的言語到了喉嚨，卻又因為離普拉特大人的尊顏太近，不禁噎了回去。
普拉特粗暴地用手繞過我的脖子，但我完全不討厭，不如說這狂野的舉動充滿了魅力。

普拉特瞥了一眼伊緹。真是的，不要在我的面前看其他女──

「要先殺掉那個女的嗎」
「欸？」

普拉特大人的手中出現紅色火球。
是擁有相當魔力的攻擊魔擊。被那個擊中的話，就算是我也沒辦法無傷了事。一般人的伊緹被擊中會怎樣自不必說。

「不、不行，普拉特大人⋯⋯」
「你是要忤逆我嗎？」
「唔⋯⋯」
「咕咕咕，魅惑果然有發揮作用。你就在旁邊看著」

然後，火球向著伊緹放出。

※

看著眼前飛來的火球，我反射性地閉上眼。

影耶被那個夢魔給魅惑，沒辦法來幫助我了。

暴風吹飛了我的身體。

可是──我卻被一股熟悉的感觸給接住。

睜開眼，身體沒有任何一處傷口。

「影耶──欸？」
「好燙燙⋯⋯沒事吧，伊緹？」

有著焦茶色頭髮的青年，接住了我的身體。
我與夢魔──普拉特之間的地板，突然出現了一塊石板。不過大部份都被爆炸給炸壊了。

他的臉龐我有印象，但卻回想不起來。我無意識地使用了情報魔法。

「陰矢⋯⋯先生？」
「欸，嘛⋯⋯那個，好久不見？」

他一邊不合時宜地對我打招呼，一邊面對普拉特，把我藏在他的背後。我環顧四周，都沒有發現影耶。

困惑的普拉特對著陰矢詢問。

「你這傢伙是誰⋯⋯？那個女的跑哪裡去了？」
「呃，其實我記憶也有點混亂⋯⋯伊緹，現在是什麼情形？」
「啊⋯⋯陰矢先生，那個惡魔是這個障壁塔的頭目魔物──色帝男淫魔的普拉特！普拉特用魅惑支配了和我一起來的影耶！可是不知道為什麼影耶不見，陰矢先生取而代之出現⋯⋯」
「啊，這麼說來是⋯⋯恩？嗯嗯！？」

陰矢先生不知道為什麼突然摀住了臉，發出苦悶的聲音。

「怎麼了！？難道是普拉特的攻擊⋯⋯！？」
「不對，我什麼都沒做啊⋯⋯」
「咕哈⋯⋯唔、唔喔⋯⋯好想吐⋯⋯」

陰矢先生一臉蒼白地按住嘴巴，一邊瞪向普拉特。

「你這傢伙⋯⋯！竟然敢，竟然敢⋯⋯！可別以為你可以輕鬆地死去啊⋯⋯！」
「哈！剛才的女的就算了，像你這樣軟弱的人類，難道還以為能勝過我嗎？」

陰矢從懷中取出十字弩，並抓住箭矢。

「不要這樣，趕快逃走吧，陰矢先生！」
「沒用的，那邊的女僕。這裡張開著強力的轉移妨礙，外面還有數十隻淫魔，你們能成功逃出去的機率連萬分之一都沒有！⋯⋯是說，這種情況下你是怎樣出現的啊！」
「欸？啊，就那樣，用超強力的轉移魔法和影耶互換位置⋯⋯之類的？」
「為什麼是疑問語氣！」
「你話很多欸！總之我上了喔！」

十字弩射出弓箭。
雖然精準地射向普拉特，但就算是淫魔對物理再弱，也不可能會因為這點程度而受傷吧。

「哼，這種玩具⋯⋯咕嘎啊啊啊！？」

然而，普拉特想要用手撥開弓箭的瞬間，碰到箭矢的右手被炸到變成灰燼。

「這、這是怎麼回事！」
「是『會根據物體震動頻率調整，無關硬度破壊一切的弓箭』啊！」

普拉特一邊用治療魔法治癒右腕，一邊用左手構築魔法。

「《毀滅的罪炎》！」
「《氣體改造》！」

普拉特瞬間生成巨大火球，向著我們射過來。
然而紫電閃爍的同時，火球也被分解成成無數細小的樣子，並就這樣消散於空氣之中。

「什麼⋯⋯！？你做了什麼！」
「是『用魔法改造大氣，配置帶有魔力的不可燃氣體，就連魔法的火焰都能瞬間消失的技能』啊！吃我這招，『有著強烈的指向性，暴風只會炸飛特定對象的炸彈』！」
「從剛才開始取名就隨便過頭了吧！」
「要你管！誰有時間幫這些本來沒打算給人看的道具一個一個取帥氣的名字啊！」

普拉特發動防御結界的同時，暴風只向著他的方向炸去。
貫穿牆壁，普拉特連著結界一起被炸飛。

「⋯⋯幹、幹掉了嗎？」
「不對，還沒結束」

在牆壁被開出的洞穴後面，普拉特展開惡魔的翅膀浮游著。他的胸口閃耀著最初見到的黑色光芒。

「魔王核，連接⋯⋯！魔力波長同步完畢！超大規模魔法，開始展開⋯⋯！」
「《大規模改造》！《地形改造・壁王城》！」

普拉特放出驚人的魔力，但陰矢放出更大量的魔力。
障壁塔發出紫電的亮光讓人睜不開眼，其中閃爍過就好像覆蓋一切的黑暗光芒。兩人的攻擊就在這之中激烈衝突。

「《破滅的砲光》！」
「欸那個，壁王城之拳！」

※

障壁塔崩壊，普拉特向著地面墜落。

我和陰矢則被飛在空中的金屬龍接住，成功著陸在地面。

「⋯⋯右直拳好像被消滅了，不過他好像處理不來同時打出的左勾拳的樣子」
「問、問題在那邊嗎⋯⋯」

該怎麼說呢，陰矢先生的戰鬥方式有點粗略。和用華麗動作來迷惑並砍倒對手的影耶完全不一樣。而且還十分令人安心。

被那個巨大手腕擊中的普拉特，身體一動一動地還勉強活著的樣子。但也是風中殘燭了。

「最後一──」
「魔王核，啟動⋯⋯！」

瞬間，黑色波動席捲而來。

「──！這個攻擊究竟是⋯⋯！」
「咕哈哈⋯⋯這不是攻擊──只是餘波而已。透過我們放出的莫大魔力，魔王核終於成功啟動了⋯⋯空間整個概念都將被炸碎，不毛之地托拉尼皮亞⋯⋯不對，就連周邊國家的邊境也都會被炸飛！這個大陸也將不再是人類可以居住土地！」
「你在說什麼⋯⋯你也會死喔？！」
「沒關係，我的性命就是為了目睹一眼這道光輝⋯⋯啊啊，這比想像之中還要美麗，還要美麗啊⋯⋯！可以粉碎世界的力量竟是如此美麗⋯⋯！」

陰矢先生取出轉移道具，然而卻沒有發動。

「轉移妨礙的魔力還殘留著。就算我死了你們也逃不掉的。你的力量確實驚人，但⋯⋯你有辦法停下這個嗎？我們，馬上就可以在彼岸相見──」

接著，普拉特就這樣斷氣。

「陰、陰矢先生⋯⋯」
「真、真的有點不大妙了這⋯⋯該怎麼辦。總之伊緹先騎乘金屬龍逃跑吧。雖然不清楚轉移妨礙的魔力有多廣，但只要逃出範圍應該就可以轉移的。」

這樣說完，陰矢先生把轉移護符交給了我。接著他取出了普拉特胸口脈動著的黑色寶珠──魔王核。

「唔哇真的不妙⋯⋯真的能停下嗎⋯⋯⋯⋯⋯等下，這個，難不成其實是超級好的素材嗎？」
「讓我逃跑⋯⋯那陰矢先生要怎麼辦呢！」
「我要停下這東西的發動。《全體改造》！」

閃爍的紫電從陰矢先生的手中噴出。雖然噴出的強光幾乎要將這一帶給染上紫電的光芒，但魔王核的脈動卻還是沒有停下。魔王核就好像是要彈開陰矢先生的魔力一般，繼續放出波動。
餘波甚至不斷地傷害他的手腕，袖子破了，金屬製的腕輪也碎開，手腕所見之處都在噴血。

「⋯⋯不行的，陰矢先生，我們快逃吧！」
「呃不是啦⋯⋯我也不是什麼一定要拯救世界的性格，只是這種好到讓人不想回去的世界，要是毀滅的話我會很困擾而已喔！」
「都說不可能了！這種，可以吹風整個國家的魔力⋯⋯！不是魔王還是勇者根本都沒辦法！」
「啊，意外地好像有戲！」
「欸欸！？」

脈動的速度便快，黑白的光和強烈的餘波如同暴風般吹襲，陰矢先生的「黑髮」也被吹了起來──接著，魔王核的光輝就這樣消失了。

※

這種事情應該是光彌負責的吧，我一邊想著這些一邊看著破破爛爛的手腕。痛得要死，如果是影耶的樣子絶對已經開始大哭了。

魔力也空空如也了。好久沒有這樣子了⋯⋯⋯

衣服的袖子被吹飛，手腕到肩膀部份都露出來了，外套完全變的像是無袖背心，不過物品箱的功能還是可以正常使用。我取出藥劑一口氣喝乾。

偷偷看了一眼伊緹那邊。⋯⋯該怎麼辦啊，已經亂來到藉口都不知道怎麼找的地步了。

呆然的伊緹突然這樣嘀咕。

「『佐藤 陰矢』、『異世界人』⋯⋯？」

⋯⋯⋯

眼睛看向手腕部份。──偽裝情報的腕輪，連著袖子一起被吹飛了。

「（完蛋了⋯⋯⋯通通完蛋了⋯⋯）」

舒適的異世界生活自此結束，這之後我將要被冰山美人的女僕投以藐視的視線，然後──

「難道⋯⋯陰矢先生，是影耶的哥哥嗎！？」
「⋯⋯⋯⋯⋯⋯⋯⋯⋯⋯⋯⋯⋯⋯沒、沒錯喔！」

※

坐在金屬龍拉的碼車上，我和伊緹一同往龍公國回去。

「伊緹，那個。關於我是異世界人這件事情，你真的願意幫我隱瞞嗎？」
「欸欸，一定是有什麼理由才要隱藏起來的吧⋯⋯而且要是告訴別人使陰矢先生不高興肯定很危險」

好像被當成炸彈一樣對待啊。嘛，我自己也覺得差不多就是了。

在我用綳帶包紮受傷的手腕時，伊緹一直看著我這邊。

「⋯⋯怎麼了嗎？」
「沒、沒事⋯⋯」

伊緹馬上背過臉去。⋯⋯這、這是被討厭了嗎？畢竟讓她陷入那麼危險的情況⋯⋯⋯

「（停下爆炸的那個身影，有一點點帥氣呢⋯⋯）」
「欸？」
「⋯⋯呼呼。沒事喔。這件事就是我們和影耶三人之間的秘密」
「呃，其實還有一個人」
「⋯⋯⋯⋯」

伊緹又再背過臉去。啊啊，大概是真的被討厭了⋯⋯⋯

就這樣，我隨便找個地方和伊緹分開，用轉移回到地下室。必須處理下手的傷口和魔力的恢復問題。

龍公國的觀光就下次吧。要是剛好有什麼事情就好了──啊，不行不行，就是說這種話才會亂立FLAG的──

「陰矢，歡迎回來！你要先吃飯？先洗澡？還是──」
「要先問關於房間這看了也不知道發生什麼事情的慘狀」
「哎呀不是啦，都是迷你菲露斯不理我，想說召喚其他魔獸然後就發生了點意外」
「你這傢伙真的是⋯⋯嘛算了，我要睡了」
「欸？你的手怎麼傷痕累累！真是的，人家普通地在擔心所以給我看看！你瞧，我還拿著陰矢特製的高性能藥膏喔！」
「那個是哇沙米！你這個笨蛋！」
「你、你說誰笨蛋呀！只是搞錯一下而已！反正你也是那樣吧，又在那邊沒用地耍帥然後又搞砸了對吧！」
「⋯⋯雖然不能否定，但被你說總感覺很火大！」
「吵、吵死了！雖然不是小影耶很可惜，但已經用光魔力的陰矢什麼才不可怕呢，給我做好覺悟！」

我一邊和夏緹亞兩人吵吵鬧鬧，一邊回到了屬於我的日常。