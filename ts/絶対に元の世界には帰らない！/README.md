# novel

- title: 絶対に元の世界には帰らない！
- title_zh: 绝对不要回到原来的世界！
- author: 潮井 イタチ
- illust:
- source: https://ncode.syosetu.com/n3037eq
- cover:
- publisher: syosetu
- date:
- status: 完結
- novel_status: 0x0001

## illusts


## publishers

- syosetu

## series

- name: 絶対に元の世界には帰らない！

## preface


```
異世界にトリップした冴えない青年、佐藤陰矢。
彼は趣味に特化した生産系チート能力で変身し、美少女異界魔剣士カゲヤとして異世界を悠々自適に生きていた。
そんなある日、魔王が現れた。
魔王を倒すためにイケメンなチート勇者も現れた。異世界から。
そんな勇者は、カゲヤと出会い――彼女(?)に一目惚れして誓いを立てる。
「僕は必ず魔王を倒して、君を元の世界に送り帰してみせる！」
（頼むからやめて！）
自分がカゲヤだとバラすわけにもいかない陰矢は、快適な異世界生活を死守するため、どさくさに紛れて勇者を倒すことを決意するのだった。
```

## tags

- R15
- 異世界転移
- syosetu
- ラブコメ ギャグ
- 男主人公
- 勇者
- 魔王
- 西洋
- チート
- 魔法
- TS
- TSF
- 女体化
- 勘違い
- 性転換
- 生産チート

# contribute

- 兴国物语

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n3037eq

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n3037eq&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n3037eq)


