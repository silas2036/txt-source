# novel

- title: 少女と戦争
- title_zh1:
- author: 長月あきの
- illust:
- source: http://ncode.syosetu.com/n3771ci/
- source_old: http://ncode.syosetu.com/n9529ec/
- cover:
- publisher: syosetu
- date: 2018-09-15T21:52:00+08:00
- status: 完結済
- novel_status: 0x0001

## illusts


## publishers

- syosetu

## series

- name: 「少女と戦争」シリーズ

## preface


```
那名少女是位稀世的英雄
那名少女是軍師，是政治家，是料理家，是音樂家，是建築家，是商人，是發明家。
那名少女謙虛又傲慢，大膽卻又心細，天真又狡猾，追求理想但卻也是現實主義
那名少女……只是個「原」不足為奇的工薪族(男)而已
這是一個少女(？)糊里糊塗地進了異世界，一手把弄戰爭的故事

その少女は稀代の英雄であった。
その少女は偉大な軍略家であり、政治家であり、料理家であり、音楽家であり、建築家であり、商人であり、発明家であった。
その少女は謙虚で傲慢、大胆にして小心、純粋で狡猾、理想を追い求める現実主義であった。
その少女は……元しがない会社員の男であった。
これは迷い込んだ異世界で戦乱に翻弄される一人の少女？の物語。
※登場人物の紹介はシリーズ化して別で作ってあります。目次の上下にあるリンクから飛ぶ事ができます。原則毎週土曜日に更新します。
```

## tags

- node-novel
- TS
- syosetu
- ……異世界転移？
- オリジナル戦記
- チートなし
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- 三人称
- 中世
- 内政
- 完結済
- 性転換
- 戦い
- 戦争
- 戦略と戦術
- 戦記
- 政治・経済
- 残酷な描写あり
- 異世界転移
- 軍隊

# contribute

- 天空De铃音
- BrianZSYJ
- 莉利娅哒哟
- 


# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id: s4931c
- novel_id: n3771ci

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n3771ci&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n3771ci/)
- [「少女と戦争」シリーズ](http://ncode.syosetu.com/s4931c/)
- 莉利娅哒哟 https://pan.baidu.com/s/1tQw_svknoR0RFjOwsW3GFA IV2Y
- [舊版和新版對比](https://tieba.baidu.com/p/5689740776 "舊版和新版對比")
- https://www.acgnz.cc/21696
- https://tieba.baidu.com/p/5152608056
- 


