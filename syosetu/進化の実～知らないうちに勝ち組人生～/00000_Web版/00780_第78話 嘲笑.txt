﻿
隔天早上，我梳洗完畢，在女子宿舍附近與奧利嘉醬匯合，馬上往職員室去，在那裡只有貝阿朵莉絲桑的身影。

「早上好，貝阿朵莉絲桑。」
「......早上好。」
「阿，早上好，誠一桑、奧利嘉醬。」
「那個...其他的老師呢？」
「大家，已經出發去教室了喲。」
「嗯！？我，難道說遲到了嗎？」

從宿舍出來應該確認過時間，怎麼會遲到了？
突然感到不安，貝阿朵莉絲桑邊笑邊告訴了我。

「不要緊。只是，這個時期哪個教室都是一大早開始進行特訓。」
「特訓嗎...到底是為什麼？」
「這個時期，不管怎樣學校都變得匆匆忙忙。例如，最近將舉辦所謂的校內對抗賽。
所以，不管哪一班都為了取得比賽資格全部都開始特訓。」
「欸！？那麼我們班也做嗎？」
「是這樣的，由於誠一桑才剛來，到那裡還顧不到...對不起。」
「不，這邊才是對不起！在這麼重要的時期來！」
「沒關係，全部都是學園長不好。」

貝阿朵莉絲桑，用很好的笑容斷言。抱歉，巴納桑。大概會有很多話想說吧。

「不過，為了能讓成績稍微好點，我們也不加油不行啊。怎樣做？從明天也開始特訓嗎？」
「這樣啊......這種事情是越早越好，就這麼辦吧。」
「那麼，今天的班會上就這樣通知吧。」

那樣說完，貝阿朵莉絲桑開始整理起手邊的資料。

「那個......這是什麼？」
「這個嗎？這是F班個人用的學習資料。」
「欸？」
「像剛才說的那樣，這個時期真的很匆忙，校內對抗賽結束後，馬上就要舉行定期測驗。」
「嗯？但是之前，考卷還沒交回來？」
「那是上課中的測驗。不過，這次是不同的。F班，有個性的孩子有很多...。」

貝阿朵莉絲桑，眼光放得很遠。嗯，主要是阿古諾斯與里昂...嗎，這二個。
...啊，只有這二個人！莎莉亞他們怎麼辦！？學習之類的有做嗎！？特別是擔心露露妮......

「那個...莎莉亞他們該怎麼辦？」
「這件事，這次會免除莎莉亞他們的測驗，請放心吧。」
「是嗎...這麼說來，雖然是說個人用的資料但是...。」
「是的，把各自不擅長的地方整理出來和，擅長的地方與應用的資料一人一份準備好了，每次都會分發哦。」
「.........。」

我真覺得貝阿朵莉絲桑的工作真厲害。
人數少的話也有可能，像這樣一個學生準備一份資料的老師，地球上也不太會有吧。
不如說，無意間對話中注意到，不知不覺從接受考試的一方變成考試的一方...怎麼說呢，
雖然不是我做的測驗，看大家被測驗折磨的情況...如何，稍微很有趣阿。
總覺得稍微能窺視到我不知道的一面，貝阿朵莉絲桑資料整理結束了。

「對不起，讓你久等了。那麼，走吧。」
「好的。」
「...嗯。」

貝阿朵莉絲桑的努力不回應不行啊。
這樣一邊想著一邊朝著F班教室去，從對面恐怕是為了校內對抗賽結束訓練的學生和老師的集團來了。
我們靠牆邊打算等待對方先過去，對方的老師在我們的前面停下來了。
對方的老師，肩膀上被切齊的清爽的淡黃色頭髮，同樣顏色的眼睛。
雖然我覺得是帥哥，實在很在意嘿嘿嘿笑的身影。
憂鬱模式之類的，只是覺得很厲害的帥哥阿...大概就結束了，這個人有著非常討人厭的感覺。
服裝，披著豪華裝飾的紅色披風，下面穿著類似軍服的東西。
......那是？那個軍服，好像在哪裡見過......啊啊，在王都特魯貝魯的時候，
和露露妮一起約會時參加的大胃王大賽，從凱薩帝國來的蘇夏克選手穿的一樣。
也就是說，這個老師也是凱薩帝國出身的？想到那樣的事，對方的老師也加深了笑容。

「哎呀，想著是誰......這不是在無能集團作夢的貝阿朵莉絲老師嗎。」

欸，什麼！？這個無謂說明語氣的人。

「早上好，克利夫老師，您說的話，他們絕對不是無能，請更正。」

這時，貝阿朵莉絲桑有點扭曲的表情，打過招呼後，用著堅毅的態度說了話。
不過，與貝阿朵莉絲桑的心情相反，對方的老師靠近了貝阿朵莉絲桑。

「哎呀，你也是頑固的人。無能稱呼為無能有什麼不好？那樣的無能丟開不管...如何？今晚，一起吃飯吧...。」
「我拒絕。」
「...原來如此...你到底還是F斑的，是我所不知道的非常優秀的啊。
這次的校內對抗賽，定期測驗想必一定是有好的結果吧。」
「啊！」

在對方老師說的瞬間，在稍微遠的地方等待的對方學生們開始竊笑起來。
話說，貝阿朵莉絲桑說了憧憬著教師之間的用餐，作為女性果然還是被邀請的啊。
那是美人，所以理所當然。雖然他也有幾次被邀請，一想到對方的事......嗯，大概是因為性格的原因才拒絕的吧！
其中，對方的老師是看起來懊悔的咬起嘴唇在貝阿朵莉絲桑的下巴添上了手指，使之抬起臉來，
這是，所謂的抬下巴嗎？很久以前在地球上流行的東西。

「現在也在做著無能的夢嗎？馬上，讓你看見現實──---。」
「那個...需要幫忙嗎？」
「誠一桑！？」

我，忍不住抓住對方老師的手，將貝阿朵莉絲桑拉開了。這時，對方老師瞬間浮現出驚訝的表情後，馬上就盯著看了。

「誰啊？你這小子...難道是在知道我是凱薩帝國的貴族而做出粗暴的行為嗎？」
「不是，不可能會知道的吧。我，與你是初次見面......。」

啊，難道說是，我不知道而已是了不起的有名人嗎？就是說，凱薩帝國出身是猜中了。
嘛，這種事怎麼樣都好......。

「雖然這麼說，有特意在對方的下巴添上手指的必要性嗎？」
「......噗哧。」

從旁邊傳來很小聲的噗哧聲，不由得將視線轉過去，奧莉嘉醬用雙手摀著嘴，噗嚕噗嚕的顫抖著。
對我的指責，對方的老師滿臉通紅。但是，立刻就恢復平靜，用傲慢無禮的態度說話。

「好像......稍微變熱了啊。即使如此...忌妒是不好的。
多少你的臉雖然是難看，對我的行動發牢騷不奇怪嗎？」
「......噗噗哧。」

奧莉嘉醬，沒能忍耐住笑。

「不不不，一般來說你的行動才奇怪吧？」

欸，難道我搞錯了嗎？突然做出壁昸的事，抬下巴的事太奇怪了吧？
但考量到僅限於帥哥的話，翔太之類的，或者是布魯托他們的話......

「姑且，你的下巴也不是不可以。」
「什...！？」
「......已經......不行了...。」

不知不覺把想說的話說出來，奧莉嘉醬終於忍耐不住，在地面拍拍的敲著。
反應好可愛啊。但是，因為失禮所以停止。

「什麼莫名其妙的話......！那樣你這傢伙，到底是誰！？」
「啊，哪個......他，是現在擔任F班班主任的誠一桑。」
「什麼？......啊啊，這傢伙就是學園長說的新老師的一人嗎？」

貝阿朵莉絲桑這樣介紹完，從上到下狠狠地盯著看後，用鼻子在笑。

「哈哈。這麼難看的人是老師......這個學園也跌到谷底了啊。」

難看的？我這樣...子，雖然是最高等級的裝備品不過......。
我的技能「千里眼」也沒有發動的樣子，對方也沒有對我的服裝使用「鑑定」。
不能使用的嗎？「鑑定」的技能。還是，使用也不會嗎？

「校內對抗賽......今年比去年能更輕鬆地打倒了啊。不，在那之前能不能出場都很可疑不是嗎？
不管怎麼說，因為只是出場就會丟臉！」

這樣說完，四周的學生都開始笑了起來。

「.........。」
「哼，什麼都無法反駁嗎？因為是事實。......哎呀，不知不覺說的入神了。那麼，就讓我這樣告辭了。
不管怎麼說，我和你們是不同的，是很忙的。」

已經沒有想說的事情嗎，就這樣滿足的表情下對方的老師帶著自己的學生回去了。
那些學生中，稍微像那布魯托的金髮學生，不知為何猛烈地盯著我看。我，做了什麼嗎？
想著那樣的事，貝阿朵莉絲桑就向我道歉了。

「對不起......那個，謝謝你的幫忙。」
「啊，不會，別在意。普通對方的行動不明白有什麼意義。還有，那個人是誰？」
「那個人，是在這個學園裡聚集了上位者的S班擔任班主任的克里夫老師。
真的非常對不起......讓誠一桑，感到了不愉快...。」
「完全沒問題。啊，我們也去教室吧。差不多快到班會時間了？」
「欸？」
「嗯？」

不知為何，貝阿朵莉絲桑吃驚的看著我。

「那個......就這樣？被那樣當成傻瓜？」
「？那又怎麼了？」
「......不會不甘心！？我有多傻都無所謂。但是...不允許把F班的大家當做笨蛋......！」
「不甘心什麼的，有什麼關係。」
「欸？」
「我知道F班的大家是優秀的，而克里夫老師不知道F班的大家是優秀的。只是這樣而已。」
「但是...！」
「這樣不是很好嗎，原本對S班和克里夫老師就不感興趣，對不感興趣的東西思考太浪費了。我會讓F班的大家變得更強而已。」
「.........。」

對我說的話，貝阿朵莉絲桑無言了。

「別說不在意別人的評價。因為評價也很重要。但是，一個一個的跟笨蛋當對手，我還不是大人呢，那比什麼都麻煩。」
「......一個人在大驚小怪。」

奧莉嘉醬，小小聲的說了。

「就是這樣，我們F班的大家全力以赴不就好了嗎？啊，總覺得不是很明白吧，抱歉。」
「不...說的也是，我們只要做好我們能做的事情就好了吧。」
「嗯嗯，沒事的，貝阿朵莉絲桑。F班的大家一定，會回應我們的期待的。」
「好的！」

一邊進行這樣的對話，我們一起向教室走去。
但是，放鬆的我們，沒能發現那時候在附近側耳偷聽的存在

