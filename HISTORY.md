# HISTORY

## 2020-08-11

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 26, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-08-08

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 25, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-08-06

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 24, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-08-05

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 23, add: 3 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 121, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 3 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2020-08-04

### Epub

- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 7 , c: 61, add: 1 )

## 2020-07-31

### Epub

- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 7 , c: 60, add: 3 )

### Segment

- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 1 )

## 2020-07-22

### Epub

- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 7 , c: 57, add: 1 )

### Segment

- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 1 )

## 2020-07-21

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 120, add: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 7 , c: 56, add: 1 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 1 )

## 2020-07-15

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 119, add: 0 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 7 , c: 55, add: 1 )

## 2020-07-05

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 119, add: 0 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 7 , c: 54, add: 1 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 1 )

## 2020-07-04

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 119, add: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 7 , c: 53, add: 3 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )
- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( s: 1 )



